#ifndef IKAFB_H
#define IKAFB_H

#include <stdint.h>
#include <stdbool.h>

struct ikafb_window;
struct ikafb_callbacks
{
  // Called when the window is exposed. The whole window should be redrawn.
  void (*expose_callback)(struct ikafb_window *window, void *param,
                          int32_t reserved);
  void *expose_callback_param;
  // Called when the window is moved, resized, or scale factor changed. status
  // contains some IKAFB_WINDOW_* flags indicating the properties changed. The
  // window should be redrawn if resized or scale factor changed.
  void (*moveresize_callback)(struct ikafb_window *window, void *param,
                              int32_t x, int32_t y, int32_t w, int32_t h,
                              uint32_t status);
  void *moveresize_callback_param;
  // Called when a key is pressed/released. status contains the current active
  // modifier keys, and whether the key is pressed or released
  // (IKAFB_IS_PRESSED).
  void (*keyboard_callback)(struct ikafb_window *window, void *param,
                            uint32_t key, uint32_t status);
  void *keyboard_callback_param;
  // Called when some text is typed.
  void (*textinput_callback)(struct ikafb_window *window, void *param,
                             uint32_t codepoint);
  void *textinput_callback_param;
  // Called when a mouse button is pressed/released. status contains the current
  // active modifier keys, and whether the button is pressed or released
  // (IKAFB_IS_PRESSED).
  void (*mouse_button_callback)(struct ikafb_window *window, void *param,
                                uint32_t button, uint32_t status);
  void *mouse_button_callback_param;
  // Called when the mouse is moved.
  void (*mouse_move_callback)(struct ikafb_window *window, void *param,
                              int32_t x, int32_t y);
  void *mouse_move_callback_param;
  // Called when a file is dropped on the window. Currently not working on
  // Wayland.
  void (*drag_and_drop_callback)(struct ikafb_window *window, void *param,
                                 char *filename);
  void *drag_and_drop_callback_param;
  // Called when the window is closed.
  void (*close_callback)(struct ikafb_window *window, void *param,
                         int32_t reserved);
  void *close_callback_param;
};

enum
{
  IKAFB_MOUSE_NONE,
  IKAFB_MOUSE_LEFT,
  IKAFB_MOUSE_RIGHT,
  IKAFB_MOUSE_MIDDLE,
  IKAFB_MOUSE_SCROLLUP,
  IKAFB_MOUSE_SCROLLDOWN,
};

enum
{
  IKAFB_KEY_UNKNOWN = -1,

  IKAFB_KEY_SPACE = 32,
  IKAFB_KEY_APOSTROPHE = 39,
  IKAFB_KEY_MULTIPLY = 42,
  IKAFB_KEY_ADD = 43,
  IKAFB_KEY_COMMA = 44,
  IKAFB_KEY_MINUS = 45,
  IKAFB_KEY_PERIOD = 46,
  IKAFB_KEY_SLASH = 47,
  IKAFB_KEY_0 = 48,
  IKAFB_KEY_1 = 49,
  IKAFB_KEY_2 = 50,
  IKAFB_KEY_3 = 51,
  IKAFB_KEY_4 = 52,
  IKAFB_KEY_5 = 53,
  IKAFB_KEY_6 = 54,
  IKAFB_KEY_7 = 55,
  IKAFB_KEY_8 = 56,
  IKAFB_KEY_9 = 57,
  IKAFB_KEY_SEMICOLON = 59,
  IKAFB_KEY_EQUAL = 61,
  IKAFB_KEY_A = 65,
  IKAFB_KEY_B = 66,
  IKAFB_KEY_C = 67,
  IKAFB_KEY_D = 68,
  IKAFB_KEY_E = 69,
  IKAFB_KEY_F = 70,
  IKAFB_KEY_G = 71,
  IKAFB_KEY_H = 72,
  IKAFB_KEY_I = 73,
  IKAFB_KEY_J = 74,
  IKAFB_KEY_K = 75,
  IKAFB_KEY_L = 76,
  IKAFB_KEY_M = 77,
  IKAFB_KEY_N = 78,
  IKAFB_KEY_O = 79,
  IKAFB_KEY_P = 80,
  IKAFB_KEY_Q = 81,
  IKAFB_KEY_R = 82,
  IKAFB_KEY_S = 83,
  IKAFB_KEY_T = 84,
  IKAFB_KEY_U = 85,
  IKAFB_KEY_V = 86,
  IKAFB_KEY_W = 87,
  IKAFB_KEY_X = 88,
  IKAFB_KEY_Y = 89,
  IKAFB_KEY_Z = 90,
  IKAFB_KEY_LEFT_BRACKET = 91,
  IKAFB_KEY_BACKSLASH = 92,
  IKAFB_KEY_RIGHT_BRACKET = 93,
  IKAFB_KEY_GRAVE_ACCENT = 96,

  IKAFB_KEY_ESCAPE = 256,
  IKAFB_KEY_ENTER = 257,
  IKAFB_KEY_TAB = 258,
  IKAFB_KEY_BACKSPACE = 259,
  IKAFB_KEY_INSERT = 260,
  IKAFB_KEY_DELETE = 261,
  IKAFB_KEY_RIGHT = 262,
  IKAFB_KEY_LEFT = 263,
  IKAFB_KEY_DOWN = 264,
  IKAFB_KEY_UP = 265,
  IKAFB_KEY_PAGE_UP = 266,
  IKAFB_KEY_PAGE_DOWN = 267,
  IKAFB_KEY_HOME = 268,
  IKAFB_KEY_END = 269,
  IKAFB_KEY_CAPS_LOCK = 280,
  IKAFB_KEY_SCROLL_LOCK = 281,
  IKAFB_KEY_NUM_LOCK = 282,
  IKAFB_KEY_PRINT_SCREEN = 283,
  IKAFB_KEY_PAUSE = 284,
  IKAFB_KEY_F1 = 290,
  IKAFB_KEY_F2 = 291,
  IKAFB_KEY_F3 = 292,
  IKAFB_KEY_F4 = 293,
  IKAFB_KEY_F5 = 294,
  IKAFB_KEY_F6 = 295,
  IKAFB_KEY_F7 = 296,
  IKAFB_KEY_F8 = 297,
  IKAFB_KEY_F9 = 298,
  IKAFB_KEY_F10 = 299,
  IKAFB_KEY_F11 = 300,
  IKAFB_KEY_F12 = 301,
  IKAFB_KEY_F13 = 302,
  IKAFB_KEY_F14 = 303,
  IKAFB_KEY_F15 = 304,
  IKAFB_KEY_F16 = 305,
  IKAFB_KEY_F17 = 306,
  IKAFB_KEY_F18 = 307,
  IKAFB_KEY_F19 = 308,
  IKAFB_KEY_F20 = 309,
  IKAFB_KEY_F21 = 310,
  IKAFB_KEY_F22 = 311,
  IKAFB_KEY_F23 = 312,
  IKAFB_KEY_F24 = 313,
  IKAFB_KEY_F25 = 314,
  IKAFB_KEY_KP_0 = 320,
  IKAFB_KEY_KP_1 = 321,
  IKAFB_KEY_KP_2 = 322,
  IKAFB_KEY_KP_3 = 323,
  IKAFB_KEY_KP_4 = 324,
  IKAFB_KEY_KP_5 = 325,
  IKAFB_KEY_KP_6 = 326,
  IKAFB_KEY_KP_7 = 327,
  IKAFB_KEY_KP_8 = 328,
  IKAFB_KEY_KP_9 = 329,
  IKAFB_KEY_KP_DECIMAL = 330,
  IKAFB_KEY_KP_DIVIDE = 331,
  IKAFB_KEY_KP_MULTIPLY = 332,
  IKAFB_KEY_KP_SUBTRACT = 333,
  IKAFB_KEY_KP_ADD = 334,
  IKAFB_KEY_KP_ENTER = 335,
  IKAFB_KEY_KP_EQUAL = 336,
  IKAFB_KEY_LEFT_SHIFT = 340,
  IKAFB_KEY_LEFT_CONTROL = 341,
  IKAFB_KEY_LEFT_ALT = 342,
  IKAFB_KEY_LEFT_SUPER = 343,
  IKAFB_KEY_RIGHT_SHIFT = 344,
  IKAFB_KEY_RIGHT_CONTROL = 345,
  IKAFB_KEY_RIGHT_ALT = 346,
  IKAFB_KEY_RIGHT_SUPER = 347,
  IKAFB_KEY_MENU = 348
};

enum
{
  IKAFB_MOD_SHIFT = 0x0001,
  IKAFB_MOD_CONTROL = 0x0002,
  IKAFB_MOD_ALT = 0x0004,
  IKAFB_MOD_SUPER = 0x0008,
  IKAFB_MOD_CAPS_LOCK = 0x0010,
  IKAFB_MOD_NUM_LOCK = 0x0020,
  IKAFB_IS_PRESSED = 0x0040,
};
#define IKAFB_PRESSED(s) ((s)&IKAFB_IS_PRESSED)
#define IKAFB_ISSCROLL(s) \
  ((s) == IKAFB_MOUSE_SCROLLDOWN || (s) == IKAFB_MOUSE_SCROLLUP)

enum
{
  IKAFB_WINDOW_RESIZED = 0x0001,
  IKAFB_WINDOW_MOVED = 0x0002,
  IKAFB_WINDOW_SCALE_CHANGED = 0x0004,
};

enum
{
  IKAFB_RESULT_SUCCESS = 0,
  // Returned if the window is closed.
  IKAFB_RESULT_WINDOW_CLOSED = 1,
  // Returned if the window is NULL.
  IKAFB_RESULT_UNINITIALIZED = 2,
  // Returned if ikafb_process_events() processed no events.
  IKAFB_RESULT_NO_EVENT = 3,
  // Returned if some error occured.
  IKAFB_RESULT_ERROR = 4,
};

enum
{
  IKAFB_CW_RESIZABLE = 0x01,
  IKAFB_CW_FULLSCREEN = 0x02,
  IKAFB_CW_BORDERLESS = 0x08,
  IKAFB_CW_ALWAYS_ON_TOP = 0x10,
  IKAFB_CW_TRANSPARENT = 0x20,
  IKAFB_CW_CENTERED = 0x40,
  // The program can handle runtime scale factor change.
  IKAFB_CW_SCALE_CHANGEABLE = 0x80,
};

// Create a window with the specified IKAFB_CW_* flags.
int32_t ikafb_create_window(struct ikafb_window **window, const char *title,
                            int32_t x, int32_t y, int32_t w, int32_t h,
                            uint32_t flags);
// Destroy the window.
void ikafb_destroy_window(struct ikafb_window *window);
// Process pending events and invoke registered callbacks.
int32_t ikafb_process_events(struct ikafb_window *window);
// Copy pixels in x, y, w, h rectangular area in the buffer to window at dst_x,
// dst_y, and update window content. The buffer contains pixels in interleaved
// B, G, R, A byte sequences.
int32_t ikafb_put_buffer_to_window(struct ikafb_window *window, int32_t dst_x,
                                   int32_t dst_y, void *buffer, int32_t x,
                                   int32_t y, int32_t w, int32_t h,
                                   uint32_t stride);

// Set the window title encoded in UTF-8.
void ikafb_set_window_title(struct ikafb_window *window, const char *title);
// Accept/reject drag and drop for the window. Currently not implemented for
// Wayland.
void ikafb_accept_drag_and_drop(struct ikafb_window *window, bool accept);
// Get the current window position and size.
void ikafb_get_window_position(struct ikafb_window *window, int32_t *x,
                               int32_t *y, int32_t *w, int32_t *h);
// Set the current window position and size.
void ikafb_set_window_position(struct ikafb_window *window, int32_t x,
                               int32_t y, int32_t w, int32_t h);
// Put the window on top of Z-order.
void ikafb_raise_window(struct ikafb_window *window);
// Begin dragging the window in the preferred method of the platform. Can only
// be called inside left mouse button down callback.
void ikafb_begin_drag_window(struct ikafb_window *window);
// Get the current mouse position in window.
void ikafb_get_mouse_position(struct ikafb_window *window, int32_t *x,
                              int32_t *y);
// Get the screen size. The exact value differs across platforms.
void ikafb_get_screen_size(struct ikafb_window *window, int32_t *w, int32_t *h);
// Set the IME candidate window position. Windows only.
void ikafb_set_ime_position(struct ikafb_window *window, int32_t x, int32_t y);
// Get the scale factor. Should be called when IKAFB_WINDOW_SCALE_CHANGED to get
// the new scale factor of the window. Can also be called with window set to
// NULL to get a scale factor for programs that can't handle scale change.
void ikafb_get_scale_factor(struct ikafb_window *window, float *scale);

// Set the callbacks.
void ikafb_get_callbacks(struct ikafb_window *window,
                         struct ikafb_callbacks *callbacks);
// Get the callbacks.
void ikafb_set_callbacks(struct ikafb_window *window,
                         struct ikafb_callbacks *callbacks);

// Set the clipboard text encoded in UTF-8.
int32_t ikafb_set_clipboard_text(struct ikafb_window *window, const char *text);
// Get the clipboard text encoded in UTF-8.
char *ikafb_get_clipboard_text(struct ikafb_window *window);
// Whether the clipboard has text.
bool ikafb_has_clipboard_text(struct ikafb_window *window);

#endif
