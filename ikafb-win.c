#define WIN32_LEAN_AND_MEAN
#define UNICODE
#include <windows.h>
#include <shellapi.h>
#include "ikafb.h"
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <stdint.h>
#include <stdbool.h>
#include <unistd.h>
#include <limits.h>

#ifndef USER_DEFAULT_SCREEN_DPI
#define USER_DEFAULT_SCREEN_DPI 96
#endif

LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);

struct ikafb_platform_data
{
  HWND window;
  LONG style;
  WNDCLASS wc;
  HDC hdc;
  BITMAPINFO *bmi;

  float wheel_value;
  float scale;
  bool scale_changeable;
  bool scale_changed;
  WCHAR high_surrogate;
};

struct ikafb_window
{
  struct ikafb_platform_data data;
  struct ikafb_callbacks callbacks;

  int32_t window_x;
  int32_t window_y;
  int32_t window_w;
  int32_t window_h;
  int32_t mouse_x;
  int32_t mouse_y;
  int32_t ime_x;
  int32_t ime_y;
  int16_t keycodes[256];
  uint32_t mods;

  bool close;
};

struct ikafb_keysym_map
{
  int keysym;
  int16_t value;
};

static const struct ikafb_keysym_map s_ikafb_win_keysym_map[] = {
  {0x30, IKAFB_KEY_0},
  {0x31, IKAFB_KEY_1},
  {0x32, IKAFB_KEY_2},
  {0x33, IKAFB_KEY_3},
  {0x34, IKAFB_KEY_4},
  {0x35, IKAFB_KEY_5},
  {0x36, IKAFB_KEY_6},
  {0x37, IKAFB_KEY_7},
  {0x38, IKAFB_KEY_8},
  {0x39, IKAFB_KEY_9},
  {0x41, IKAFB_KEY_A},
  {0x42, IKAFB_KEY_B},
  {0x43, IKAFB_KEY_C},
  {0x44, IKAFB_KEY_D},
  {0x45, IKAFB_KEY_E},
  {0x46, IKAFB_KEY_F},
  {0x47, IKAFB_KEY_G},
  {0x48, IKAFB_KEY_H},
  {0x49, IKAFB_KEY_I},
  {0x4A, IKAFB_KEY_J},
  {0x4B, IKAFB_KEY_K},
  {0x4C, IKAFB_KEY_L},
  {0x4D, IKAFB_KEY_M},
  {0x4E, IKAFB_KEY_N},
  {0x4F, IKAFB_KEY_O},
  {0x50, IKAFB_KEY_P},
  {0x51, IKAFB_KEY_Q},
  {0x52, IKAFB_KEY_R},
  {0x53, IKAFB_KEY_S},
  {0x54, IKAFB_KEY_T},
  {0x55, IKAFB_KEY_U},
  {0x56, IKAFB_KEY_V},
  {0x57, IKAFB_KEY_W},
  {0x58, IKAFB_KEY_X},
  {0x59, IKAFB_KEY_Y},
  {0x5A, IKAFB_KEY_Z},

  {VK_OEM_7, IKAFB_KEY_APOSTROPHE},
  {VK_OEM_5, IKAFB_KEY_BACKSLASH},
  {VK_OEM_COMMA, IKAFB_KEY_COMMA},
  {VK_OEM_PLUS, IKAFB_KEY_EQUAL},
  {VK_OEM_3, IKAFB_KEY_GRAVE_ACCENT},
  {VK_OEM_4, IKAFB_KEY_LEFT_BRACKET},
  {VK_OEM_MINUS, IKAFB_KEY_MINUS},
  {VK_OEM_PERIOD, IKAFB_KEY_PERIOD},
  {VK_OEM_6, IKAFB_KEY_RIGHT_BRACKET},
  {VK_OEM_1, IKAFB_KEY_SEMICOLON},
  {VK_OEM_2, IKAFB_KEY_SLASH},

  {VK_BACK, IKAFB_KEY_BACKSPACE},
  {VK_DELETE, IKAFB_KEY_DELETE},
  {VK_END, IKAFB_KEY_END},
  {VK_RETURN, IKAFB_KEY_ENTER},
  {VK_ESCAPE, IKAFB_KEY_ESCAPE},
  {VK_HOME, IKAFB_KEY_HOME},
  {VK_INSERT, IKAFB_KEY_INSERT},
  {VK_APPS, IKAFB_KEY_MENU},
  {VK_NEXT, IKAFB_KEY_PAGE_DOWN},
  {VK_PRIOR, IKAFB_KEY_PAGE_UP},
  {VK_PAUSE, IKAFB_KEY_PAUSE},
  {VK_SPACE, IKAFB_KEY_SPACE},
  {VK_TAB, IKAFB_KEY_TAB},
  {VK_CAPITAL, IKAFB_KEY_CAPS_LOCK},
  {VK_NUMLOCK, IKAFB_KEY_NUM_LOCK},
  {VK_SCROLL, IKAFB_KEY_SCROLL_LOCK},
  {VK_F1, IKAFB_KEY_F1},
  {VK_F2, IKAFB_KEY_F2},
  {VK_F3, IKAFB_KEY_F3},
  {VK_F4, IKAFB_KEY_F4},
  {VK_F5, IKAFB_KEY_F5},
  {VK_F6, IKAFB_KEY_F6},
  {VK_F7, IKAFB_KEY_F7},
  {VK_F8, IKAFB_KEY_F8},
  {VK_F9, IKAFB_KEY_F9},
  {VK_F10, IKAFB_KEY_F10},
  {VK_F11, IKAFB_KEY_F11},
  {VK_F12, IKAFB_KEY_F12},
  {VK_F13, IKAFB_KEY_F13},
  {VK_F14, IKAFB_KEY_F14},
  {VK_F15, IKAFB_KEY_F15},
  {VK_F16, IKAFB_KEY_F16},
  {VK_F17, IKAFB_KEY_F17},
  {VK_F18, IKAFB_KEY_F18},
  {VK_F19, IKAFB_KEY_F19},
  {VK_F20, IKAFB_KEY_F20},
  {VK_F21, IKAFB_KEY_F21},
  {VK_F22, IKAFB_KEY_F22},
  {VK_F23, IKAFB_KEY_F23},
  {VK_F24, IKAFB_KEY_F24},
  {VK_MENU, IKAFB_KEY_LEFT_ALT},
  {VK_CONTROL, IKAFB_KEY_LEFT_CONTROL},
  {VK_SHIFT, IKAFB_KEY_LEFT_SHIFT},
  {VK_LMENU, IKAFB_KEY_LEFT_ALT},
  {VK_LCONTROL, IKAFB_KEY_LEFT_CONTROL},
  {VK_LSHIFT, IKAFB_KEY_LEFT_SHIFT},
  {VK_LWIN, IKAFB_KEY_LEFT_SUPER},
  {VK_SNAPSHOT, IKAFB_KEY_PRINT_SCREEN},
  {VK_RMENU, IKAFB_KEY_RIGHT_ALT},
  {VK_RCONTROL, IKAFB_KEY_RIGHT_CONTROL},
  {VK_RSHIFT, IKAFB_KEY_RIGHT_SHIFT},
  {VK_RWIN, IKAFB_KEY_RIGHT_SUPER},
  {VK_DOWN, IKAFB_KEY_DOWN},
  {VK_LEFT, IKAFB_KEY_LEFT},
  {VK_RIGHT, IKAFB_KEY_RIGHT},
  {VK_UP, IKAFB_KEY_UP},

  {VK_NUMPAD0, IKAFB_KEY_KP_0},
  {VK_NUMPAD1, IKAFB_KEY_KP_1},
  {VK_NUMPAD2, IKAFB_KEY_KP_2},
  {VK_NUMPAD3, IKAFB_KEY_KP_3},
  {VK_NUMPAD4, IKAFB_KEY_KP_4},
  {VK_NUMPAD5, IKAFB_KEY_KP_5},
  {VK_NUMPAD6, IKAFB_KEY_KP_6},
  {VK_NUMPAD7, IKAFB_KEY_KP_7},
  {VK_NUMPAD8, IKAFB_KEY_KP_8},
  {VK_NUMPAD9, IKAFB_KEY_KP_9},
  {VK_ADD, IKAFB_KEY_KP_ADD},
  {VK_DECIMAL, IKAFB_KEY_KP_DECIMAL},
  {VK_DIVIDE, IKAFB_KEY_KP_DIVIDE},
  {VK_MULTIPLY, IKAFB_KEY_KP_MULTIPLY},
  {VK_SUBTRACT, IKAFB_KEY_KP_SUBTRACT},
};

static int16_t
ikafb_translate_keysym(int keysym, const struct ikafb_keysym_map map[],
                       size_t size)
{
  for (size_t i = 0; i < size; ++i)
  {
    if (map[i].keysym == keysym)
      return map[i].value;
  }

  return IKAFB_KEY_UNKNOWN;
}

static void
ikafb_init_keycodes(struct ikafb_window *window)
{
  for (size_t i = 0; i < sizeof(window->keycodes) / sizeof(window->keycodes[0]);
       ++i)
    window->keycodes[i] = ikafb_translate_keysym(
      i, s_ikafb_win_keysym_map,
      sizeof(s_ikafb_win_keysym_map) / sizeof(s_ikafb_win_keysym_map[0]));
  ;
}

#include <dwmapi.h>

static void
ikafb_set_window_transparent(struct ikafb_window *window)
{
  HMODULE mod;
  mod = LoadLibrary(TEXT("dwmapi.dll"));
  INT(WINAPI * pfnDwmEnableBlurBehindWindow)(HWND, DWM_BLURBEHIND *) = NULL;
  if (mod)
  {
    pfnDwmEnableBlurBehindWindow =
      GetProcAddress(mod, "DwmEnableBlurBehindWindow");
    if (pfnDwmEnableBlurBehindWindow)
    {
      HRGN rgn = CreateRectRgn(0, 0, -1, -1);
      DWM_BLURBEHIND dbb = {0};
      dbb.dwFlags = DWM_BB_ENABLE | DWM_BB_BLURREGION;
      dbb.fEnable = TRUE;
      dbb.hRgnBlur = rgn;
      pfnDwmEnableBlurBehindWindow(window->data.window, &dbb);
      DeleteObject(rgn);
    }
    FreeLibrary(mod);
  }
}

int32_t
ikafb_create_window(struct ikafb_window **_window, const char *title, int32_t x,
                    int32_t y, int32_t w, int32_t h, uint32_t flags)
{
  if (w <= 0 || h <= 0)
  {
    return IKAFB_RESULT_ERROR;
  }

  RECT rect = {0};
  struct ikafb_window *window = calloc(1, sizeof(struct ikafb_window));
  if (!window)
  {
    return IKAFB_RESULT_ERROR;
  }

  float scale = 1;
  ikafb_get_scale_factor(NULL, &scale);
  window->data.scale = scale;
  if (flags & IKAFB_CW_SCALE_CHANGEABLE)
  {
    window->data.scale_changeable = true;
  }

  window->data.style = WS_OVERLAPPEDWINDOW & ~WS_MAXIMIZEBOX & ~WS_THICKFRAME;

  if (flags & IKAFB_CW_BORDERLESS)
  {
    window->data.style = WS_POPUP;
  }

  if (flags & IKAFB_CW_RESIZABLE)
  {
    window->data.style |= WS_MAXIMIZEBOX | WS_SIZEBOX;
  }

  if (flags & IKAFB_CW_FULLSCREEN)
  {
    window->data.style = WS_OVERLAPPEDWINDOW;

    w = GetSystemMetrics(SM_CXFULLSCREEN);
    h = GetSystemMetrics(SM_CYFULLSCREEN);

    rect.right = w;
    rect.bottom = h;
    AdjustWindowRect(&rect, window->data.style, 0);
    if (rect.left < 0)
    {
      w += rect.left * 2;
      rect.right += rect.left;
      rect.left = 0;
    }
    if (rect.bottom > (LONG)h)
    {
      h -= (rect.bottom - h);
      rect.bottom += (rect.bottom - h);
      rect.top = 0;
    }
  }
  else
  {
    rect.right = w;
    rect.bottom = h;
    AdjustWindowRect(&rect, window->data.style, 0);

    rect.right -= rect.left;
    rect.bottom -= rect.top;
    x += rect.left;
    y += rect.top;

    if (flags & IKAFB_CW_CENTERED)
    {
      x = (GetSystemMetrics(SM_CXSCREEN) - rect.right) / 2;
      y = (GetSystemMetrics(SM_CYSCREEN) - rect.bottom + rect.top) / 2;
    }
  }

  wchar_t w_title[1024];
  if (0 == MultiByteToWideChar(65001, 0, title, -1, w_title, sizeof(w_title)))
  {
    free(window);
    return IKAFB_RESULT_ERROR;
  }

  window->data.wc.style = 0;
  window->data.wc.lpfnWndProc = WndProc;
  window->data.wc.hCursor = LoadCursor(0, IDC_ARROW);
  window->data.wc.lpszClassName = w_title;
  window->data.wc.hIcon = (HICON)LoadImage(
    GetModuleHandle(0), MAKEINTRESOURCE(2), IMAGE_ICON,
    GetSystemMetrics(SM_CXICON), GetSystemMetrics(SM_CYICON), 0);

  RegisterClass(&window->data.wc);

  window->window_x = x;
  window->window_y = y;
  window->window_w = w;
  window->window_h = h;
  window->data.window =
    CreateWindowExW(0, w_title, w_title, window->data.style, x, y, rect.right,
                    rect.bottom, 0, 0, 0, 0);

  if (!window->data.window)
  {
    free(window);
    return IKAFB_RESULT_ERROR;
  }

  SetWindowLongPtr(window->data.window, GWLP_USERDATA, (LONG_PTR)window);
  if (flags & IKAFB_CW_ALWAYS_ON_TOP)
    SetWindowPos(window->data.window, HWND_TOPMOST, 0, 0, 0, 0,
                 SWP_NOMOVE | SWP_NOSIZE);

  ShowWindow(window->data.window, SW_NORMAL);
  window->data.hdc = GetDC(window->data.window);
  window->data.bmi =
    (BITMAPINFO *)calloc(1, sizeof(BITMAPINFOHEADER) + sizeof(RGBQUAD) * 4);
  if (!window->data.bmi)
  {
    DestroyWindow(window->data.window);
    free(window);
    return IKAFB_RESULT_ERROR;
  }

  window->data.bmi->bmiHeader.biSize = sizeof(BITMAPINFOHEADER);
  window->data.bmi->bmiHeader.biPlanes = 1;
  window->data.bmi->bmiHeader.biBitCount = 32;
  window->data.bmi->bmiHeader.biCompression = BI_BITFIELDS;
  window->data.bmi->bmiHeader.biWidth = w;
  window->data.bmi->bmiHeader.biHeight = -h;
  window->data.bmi->bmiColors[0].rgbRed = 0xff;
  window->data.bmi->bmiColors[1].rgbGreen = 0xff;
  window->data.bmi->bmiColors[2].rgbBlue = 0xff;

  if (flags & IKAFB_CW_TRANSPARENT)
  {
    ikafb_set_window_transparent(window);
  }

  ikafb_init_keycodes(window);
  *_window = window;
  return IKAFB_RESULT_SUCCESS;
}

#define CHECK_WINDOW_INITIALIZED() \
  if (!window)                     \
  return IKAFB_RESULT_UNINITIALIZED
#define CHECK_WINDOW_INITIALIZED_NORET() \
  if (!window)                           \
  return
void
ikafb_destroy_window(struct ikafb_window *window)
{
  CHECK_WINDOW_INITIALIZED_NORET();
  free(window->data.bmi);
  DestroyWindow(window->data.window);
  window->close = true;
  free(window);
}

int32_t
ikafb_put_buffer_to_window(struct ikafb_window *window, int32_t dst_x,
                           int32_t dst_y, void *buffer, int32_t x, int32_t y,
                           int32_t w, int32_t h, uint32_t stride)
{
  CHECK_WINDOW_INITIALIZED();
  if (window->close)
  {
    return IKAFB_RESULT_WINDOW_CLOSED;
  }

  window->data.bmi->bmiHeader.biWidth = stride / 4;
  window->data.bmi->bmiHeader.biHeight = -(y + h);
  // HACK: the function misbehaves when x = 0 (it assumes src y is 0)
  // set src y to 0 and height to y + h as an inefficient workaround
  StretchDIBits(window->data.hdc, dst_x, 0, w, y + h, x, 0, w, y + h, buffer,
                window->data.bmi, DIB_RGB_COLORS, SRCCOPY);

  return IKAFB_RESULT_SUCCESS;
}

static int
ikafb_translate_key(struct ikafb_window *window, unsigned int wParam,
                    unsigned long lParam)
{
  if (wParam == VK_CONTROL)
  {
    MSG next;
    DWORD time;

    if (lParam & 0x01000000)
      return IKAFB_KEY_RIGHT_CONTROL;

    time = GetMessageTime();
    if (PeekMessage(&next, 0x0, 0, 0, PM_NOREMOVE))
      if (next.message == WM_KEYDOWN || next.message == WM_SYSKEYDOWN
          || next.message == WM_KEYUP || next.message == WM_SYSKEYUP)
        if (next.wParam == VK_MENU && (next.lParam & 0x01000000)
            && next.time == time)
          return IKAFB_KEY_UNKNOWN;

    return IKAFB_KEY_LEFT_CONTROL;
  }

  if (wParam == VK_PROCESSKEY)
    return IKAFB_KEY_UNKNOWN;

  return window->keycodes[wParam & 0xFF];
}

static int
ikafb_translate_mod(void)
{
  int mods = 0;

  if (GetKeyState(VK_SHIFT) & 0x8000)
    mods |= IKAFB_MOD_SHIFT;
  if (GetKeyState(VK_CONTROL) & 0x8000)
    mods |= IKAFB_MOD_CONTROL;
  if (GetKeyState(VK_MENU) & 0x8000)
    mods |= IKAFB_MOD_ALT;
  if ((GetKeyState(VK_LWIN) | GetKeyState(VK_RWIN)) & 0x8000)
    mods |= IKAFB_MOD_SUPER;
  if (GetKeyState(VK_CAPITAL) & 1)
    mods |= IKAFB_MOD_CAPS_LOCK;
  if (GetKeyState(VK_NUMLOCK) & 1)
    mods |= IKAFB_MOD_NUM_LOCK;

  return mods;
}

#define callback(type, ...)                                        \
  if (window->callbacks.type)                                      \
  {                                                                \
    window->callbacks.type(window, window->callbacks.type##_param, \
                           __VA_ARGS__);                           \
  }
LRESULT CALLBACK
WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
  LRESULT res = 0;

  struct ikafb_window *window =
    (struct ikafb_window *)GetWindowLongPtr(hWnd, GWLP_USERDATA);

  switch (message)
  {
  case WM_PAINT:
  {
    callback(expose_callback, 0);
    ValidateRect(hWnd, NULL);
    break;
  }

  case WM_CLOSE:
  {
    window->close = true;
    callback(close_callback, 0);
    return res;
  }

  case WM_DESTROY:
  {
    window->close = true;
    break;
  }

  case WM_KEYDOWN:
  case WM_SYSKEYDOWN:
  case WM_KEYUP:
  case WM_SYSKEYUP:
  {
    int keycode =
      ikafb_translate_key(window, (unsigned int)wParam, (unsigned long)lParam);
    int is_pressed = !((lParam >> 31) & 1);
    window->mods = ikafb_translate_mod();

    if (keycode != IKAFB_KEY_UNKNOWN)
      callback(keyboard_callback, keycode,
               window->mods | (is_pressed ? IKAFB_IS_PRESSED : 0));

    return DefWindowProc(hWnd, message, wParam, lParam);
  }

  case WM_CHAR:
  case WM_SYSCHAR:
  {
    if (wParam >= 0xd800 && wParam <= 0xdbff)
    {
      window->data.high_surrogate = (WCHAR)wParam;
    }
    else
    {
      uint32_t codepoint = 0;
      if (wParam >= 0xdc00 && wParam <= 0xdfff)
      {
        if (window->data.high_surrogate != 0)
        {
          codepoint += (window->data.high_surrogate - 0xd800) << 10;
          codepoint += (WCHAR)wParam - 0xdc00;
          codepoint += 0x10000;
        }
      }
      else
      {
        codepoint = (WCHAR)wParam;
      }
      window->data.high_surrogate = 0;

      // prevent control characters
      if ((codepoint >= 0x0020 && codepoint <= 0x007e) || (codepoint >= 0x00a0))
      {
        callback(textinput_callback, codepoint);
      }
    }
    return DefWindowProc(hWnd, message, wParam, lParam);
  }

  case WM_UNICHAR:
  {
    if (wParam == UNICODE_NOCHAR)
    {
      return TRUE;
    }

    callback(textinput_callback, (uint32_t)wParam);
    break;
  }

  case WM_LBUTTONUP:
  case WM_RBUTTONUP:
  case WM_MBUTTONUP:
  case WM_XBUTTONUP:
  case WM_LBUTTONDOWN:
  case WM_LBUTTONDBLCLK:
  case WM_RBUTTONDOWN:
  case WM_RBUTTONDBLCLK:
  case WM_MBUTTONDOWN:
  case WM_MBUTTONDBLCLK:
  case WM_XBUTTONDOWN:
  case WM_XBUTTONDBLCLK:
  {
    uint32_t button = IKAFB_MOUSE_NONE;
    window->mods = ikafb_translate_mod();
    int is_pressed = 0;
    switch (message)
    {
    case WM_LBUTTONDOWN:
      is_pressed = 1;
      button = IKAFB_MOUSE_LEFT;
      SetCapture(hWnd);
      break;
    case WM_LBUTTONUP:
      button = IKAFB_MOUSE_LEFT;
      ReleaseCapture();
      break;
    case WM_RBUTTONDOWN:
      is_pressed = 1;
    case WM_RBUTTONUP:
      button = IKAFB_MOUSE_RIGHT;
      break;
    case WM_MBUTTONDOWN:
      is_pressed = 1;
    case WM_MBUTTONUP:
      button = IKAFB_MOUSE_MIDDLE;
      break;
    default:
      break;
    }

    if (button != IKAFB_MOUSE_NONE)
    {
      callback(mouse_button_callback, button,
               window->mods | (is_pressed ? IKAFB_IS_PRESSED : 0));
    }

    break;
  }

  case WM_MOUSEWHEEL:
  {
    float yw = (SHORT)HIWORD(wParam) / (float)WHEEL_DELTA;
    window->data.wheel_value -= yw;
    if (window->data.wheel_value >= 1)
    {
      window->data.wheel_value = 0;
      callback(mouse_button_callback, IKAFB_MOUSE_SCROLLDOWN,
               window->mods | IKAFB_IS_PRESSED);
    }
    else if (window->data.wheel_value <= -1)
    {
      window->data.wheel_value = 0;
      callback(mouse_button_callback, IKAFB_MOUSE_SCROLLUP,
               window->mods | IKAFB_IS_PRESSED);
    }

    break;
  }

  case WM_MOUSEMOVE:
  {
    window->mouse_x = (int)(short)LOWORD(lParam);
    window->mouse_y = (int)(short)HIWORD(lParam);
    callback(mouse_move_callback, window->mouse_x, window->mouse_y);
    break;
  }

  case WM_WINDOWPOSCHANGED:
  {
    if (IsIconic(hWnd))
    {
      break;
    }

    WINDOWPOS *wp = (WINDOWPOS *)lParam;
    RECT wrect = {wp->x, wp->y, wp->x + wp->cx, wp->y + wp->cy};
    RECT crect = {0};
    AdjustWindowRect(&crect, window->data.style, FALSE);
    int32_t x = wrect.left - crect.left;
    int32_t y = wrect.top - crect.top;
    int32_t w = (wrect.right - wrect.left) - (crect.right - crect.left);
    int32_t h = (wrect.bottom - wrect.top) - (crect.bottom - crect.top);

    uint32_t resized = (window->window_w != w || window->window_h != h)
                       ? IKAFB_WINDOW_RESIZED
                       : 0;
    uint32_t moved =
      (window->window_x != x || window->window_y != y) ? IKAFB_WINDOW_MOVED : 0;
    uint32_t scaled =
      (window->data.scale_changed) ? IKAFB_WINDOW_SCALE_CHANGED : 0;
    uint32_t status = resized | moved | scaled;
    window->data.scale_changed = false;
    window->window_x = x;
    window->window_y = y;
    window->window_w = w;
    window->window_h = h;
    callback(moveresize_callback, x, y, w, h, status);
    break;
  }

  // FIXME: use ForDpi functions if scale is changeable
  case WM_DPICHANGED:
  {
    if (!window->data.scale_changeable)
    {
      break;
    }

    window->data.scale = LOWORD(wParam) / (float)USER_DEFAULT_SCREEN_DPI;
    window->data.scale_changed = true;
    RECT *rect = (RECT *)lParam;
    SetWindowPos(window->data.window, HWND_TOP, rect->left, rect->top,
                 rect->right - rect->left, rect->bottom - rect->top,
                 SWP_NOZORDER);
    break;
  }

  case WM_DROPFILES:
  {
    UINT i;
    HDROP drop = (HDROP)wParam;
    UINT count = DragQueryFileW(drop, 0xFFFFFFFF, NULL, 0);
    for (i = 0; i < count; ++i)
    {
      UINT size = DragQueryFileW(drop, i, NULL, 0) + 1;
      LPWSTR buffer = calloc(sizeof(WCHAR), size);
      if (buffer)
      {
        if (DragQueryFileW(drop, i, buffer, size))
        {
          char *filename = NULL;
          int filename_len =
            WideCharToMultiByte(65001, 0, buffer, -1, filename, 0, NULL, NULL);
          filename = calloc(1, filename_len + 1);
          if (0
              != WideCharToMultiByte(65001, 0, buffer, -1, filename,
                                     filename_len, NULL, NULL))
          {
            callback(drag_and_drop_callback, filename);
          }
          free(filename);
        }
        free(buffer);
      }
    }
    DragFinish(drop);
    break;
  }

  case WM_IME_STARTCOMPOSITION:
  {
    HIMC imc = ImmGetContext(window->data.window);
    POINT pt;
    pt.x = window->ime_x;
    pt.y = window->ime_y;
    COMPOSITIONFORM cf;
    cf.dwStyle = CFS_POINT;
    cf.ptCurrentPos = pt;
    ImmSetCompositionWindow(imc, &cf);
    ImmReleaseContext(window->data.window, imc);
  }

  default:
  {
    res = DefWindowProc(hWnd, message, wParam, lParam);
    break;
  }
  }

  return res;
}

int32_t
ikafb_process_events(struct ikafb_window *window)
{
  CHECK_WINDOW_INITIALIZED();
  if (window->close)
  {
    return IKAFB_RESULT_WINDOW_CLOSED;
  }

  MSG msg;
  bool has_event = false;
  while (window->close == false && PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
  {
    TranslateMessage(&msg);
    DispatchMessage(&msg);
    has_event = true;
  }

  return has_event ? IKAFB_RESULT_SUCCESS : IKAFB_RESULT_NO_EVENT;
}

void
ikafb_set_window_title(struct ikafb_window *window, const char *title)
{
  CHECK_WINDOW_INITIALIZED_NORET();
  wchar_t w_title[1024];
  if (0 == MultiByteToWideChar(65001, 0, title, -1, w_title, sizeof(w_title)))
    return;

  SetWindowTextW(window->data.window, w_title);
}

void
ikafb_accept_drag_and_drop(struct ikafb_window *window, bool accept)
{
  CHECK_WINDOW_INITIALIZED_NORET();
  DragAcceptFiles(window->data.window, accept);
}

void
ikafb_get_window_position(struct ikafb_window *window, int32_t *x, int32_t *y,
                          int32_t *w, int32_t *h)
{
  *x = *y = *w = *h = 0;
  CHECK_WINDOW_INITIALIZED_NORET();
  *x = window->window_x;
  *y = window->window_y;
  *w = window->window_w;
  *h = window->window_h;
}

void
ikafb_get_mouse_position(struct ikafb_window *window, int32_t *x, int32_t *y)
{
  *x = *y = 0;
  CHECK_WINDOW_INITIALIZED_NORET();
  *x = window->mouse_x;
  *y = window->mouse_y;
}

void
ikafb_set_window_position(struct ikafb_window *window, int32_t x, int32_t y,
                          int32_t w, int32_t h)
{
  CHECK_WINDOW_INITIALIZED_NORET();

  RECT rect;
  rect.left = x;
  rect.top = y;
  rect.right = x + w;
  rect.bottom = y + h;
  AdjustWindowRect(&rect, window->data.style, FALSE);
  SetWindowPos(window->data.window, HWND_TOP, rect.left, rect.top,
               rect.right - rect.left, rect.bottom - rect.top, SWP_NOZORDER);
}

void
ikafb_raise_window(struct ikafb_window *window)
{
  CHECK_WINDOW_INITIALIZED_NORET();
  SetForegroundWindow(window->data.window);
}

void
ikafb_begin_drag_window(struct ikafb_window *window)
{
  CHECK_WINDOW_INITIALIZED_NORET();
  // TODO: other mouse buttons
  ReleaseCapture();
  callback(mouse_button_callback, IKAFB_MOUSE_LEFT, window->mods);
  SendMessage(window->data.window, WM_NCLBUTTONDOWN, HTCAPTION, 0);
}

void
ikafb_get_screen_size(struct ikafb_window *window, int32_t *w, int32_t *h)
{
  *w = *h = 0;
  CHECK_WINDOW_INITIALIZED_NORET();
  *w = GetSystemMetrics(SM_CXSCREEN);
  *h = GetSystemMetrics(SM_CYSCREEN);
}

void
ikafb_set_ime_position(struct ikafb_window *window, int32_t x, int32_t y)
{
  CHECK_WINDOW_INITIALIZED_NORET();
  window->ime_x = x;
  window->ime_y = y;
}

void
ikafb_get_scale_factor(struct ikafb_window *window, float *scale)
{
  *scale = 1;
  if (!window)
  {
    UINT x;
    const HDC dc = GetDC(NULL);
    x = GetDeviceCaps(dc, LOGPIXELSX);
    ReleaseDC(NULL, dc);
    *scale = x / (float)USER_DEFAULT_SCREEN_DPI;
    if (*scale == 0)
    {
      *scale = 1.0f;
    }
  }
  else
  {
    *scale = window->data.scale;
  }
}

void
ikafb_get_callbacks(struct ikafb_window *window,
                    struct ikafb_callbacks *callbacks)
{
  CHECK_WINDOW_INITIALIZED_NORET();
  *callbacks = window->callbacks;
}

void
ikafb_set_callbacks(struct ikafb_window *window,
                    struct ikafb_callbacks *callbacks)
{
  CHECK_WINDOW_INITIALIZED_NORET();
  window->callbacks = *callbacks;
}

int32_t
ikafb_set_clipboard_text(struct ikafb_window *window, const char *text)
{
  CHECK_WINDOW_INITIALIZED();
  int count;
  HANDLE object;
  WCHAR *buffer;

  count = MultiByteToWideChar(65001, 0, text, -1, NULL, 0);
  if (!count)
    return IKAFB_RESULT_ERROR;

  object = GlobalAlloc(GMEM_MOVEABLE, count * sizeof(WCHAR));
  if (!object)
  {
    return IKAFB_RESULT_ERROR;
  }

  buffer = GlobalLock(object);
  if (!buffer)
  {
    GlobalFree(object);
    return IKAFB_RESULT_ERROR;
  }

  MultiByteToWideChar(65001, 0, text, -1, buffer, count);
  GlobalUnlock(object);

  if (!OpenClipboard(window->data.window))
  {
    GlobalFree(object);
    return IKAFB_RESULT_ERROR;
  }

  EmptyClipboard();
  SetClipboardData(CF_UNICODETEXT, object);
  CloseClipboard();

  return IKAFB_RESULT_SUCCESS;
}

char *
ikafb_get_utf8_text_from_wstr(const WCHAR *source)
{
  char *target;
  int size;

  size = WideCharToMultiByte(65001, 0, source, -1, NULL, 0, NULL, NULL);
  if (!size)
  {
    return NULL;
  }

  target = calloc(size, 1);
  if (!WideCharToMultiByte(65001, 0, source, -1, target, size, NULL, NULL))
  {
    free(target);
    return NULL;
  }

  return target;
}

char *
ikafb_get_clipboard_text(struct ikafb_window *window)
{
  if (!window)
  {
    return calloc(1, 1);
  }

  HANDLE object;
  WCHAR *buffer;
  char *text;

  if (!OpenClipboard(window->data.window))
  {
    return calloc(1, 1);
  }

  object = GetClipboardData(CF_UNICODETEXT);
  if (!object)
  {
    CloseClipboard();
    return calloc(1, 1);
  }

  buffer = GlobalLock(object);
  if (!buffer)
  {
    CloseClipboard();
    return calloc(1, 1);
  }

  text = ikafb_get_utf8_text_from_wstr(buffer);
  if (!text)
  {
    return calloc(1, 1);
  }

  GlobalUnlock(object);
  CloseClipboard();

  return text;
}

bool
ikafb_has_clipboard_text(struct ikafb_window *window)
{
  bool result = false;
  char *text = ikafb_get_clipboard_text(window);
  if (text)
  {
    result = text[0] != '\0' ? true : false;
    free(text);
  }
  return result;
}
