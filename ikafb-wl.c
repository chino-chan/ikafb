#define _POSIX_C_SOURCE 200809L
#include "ikafb.h"
#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <stdbool.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <limits.h>
#include <math.h>
#include <signal.h>
#include <time.h>

#include <sys/select.h>
#include <sys/mman.h>
#include "wayland/wayland-client.h"
#include "wayland/wayland-cursor.h"
#include "wayland/xdg-shell.c"
#include "wayland/xdg-shell.h"
#include "wayland/xdg-decoration-unstable-v1.c"
#include "wayland/xdg-decoration-unstable-v1.h"
#include "wayland/viewporter.c"
#include "wayland/viewporter.h"
#include "wayland/fractional-scale-v1.c"
#include "wayland/fractional-scale-v1.h"

#define IKAFB_AUTOREPEAT_DELAY 500
#define IKAFB_AUTOREPEAT_TIME 40
#define IKAFB_WL_MAX_OUTPUTS 8
#define IKAFB_WL_MAX_SEATS 8
#define callback(type, ...)                                        \
  if (window->callbacks.type)                                      \
  {                                                                \
    window->callbacks.type(window, window->callbacks.type##_param, \
                           __VA_ARGS__);                           \
  }

static int32_t ikafb_wait_for_buffer_release(struct ikafb_window *window);

struct ikafb_platform_data
{
  struct wl_display *display;
  struct wl_registry *registry;
  struct wl_compositor *compositor;
  struct xdg_wm_base *wm_base;
  struct wl_shm *shm;
  struct wl_output *output[IKAFB_WL_MAX_OUTPUTS];
  uint32_t output_id[IKAFB_WL_MAX_OUTPUTS];
  uint32_t output_count;

  struct wl_surface *surface;
  struct xdg_surface *xdg_surface;
  struct xdg_toplevel *xdg_toplevel;
  bool surface_configured;

  struct wl_seat *seat_current;
  struct wl_seat *seat[IKAFB_WL_MAX_SEATS];
  uint32_t seat_id[IKAFB_WL_MAX_OUTPUTS];
  uint32_t seat_count;
  struct wl_keyboard *keyboard[IKAFB_WL_MAX_SEATS];
  struct wl_pointer *pointer[IKAFB_WL_MAX_SEATS];
  struct wl_cursor *cursor;
  struct wl_cursor_theme *cursor_theme;
  struct wl_surface *cursor_surface;
  struct wl_buffer *cursor_buffer;
  int32_t cursor_scale;
  double axis_value;

  struct wl_shm_pool *shm_pool;
  struct wl_buffer *shm_buffer;
  int shm_fd;
  void *shm_data;
  size_t shm_size;
  bool shm_busy;
  int32_t shm_format;

  struct wl_data_device_manager *data_device_manager;
  struct wl_data_device *data_device;
  struct wl_data_source *data_source;
  char *clipboard;
  bool selection_set;

  struct zxdg_toplevel_decoration_v1 *toplevel_decoration;
  struct zxdg_decoration_manager_v1 *decoration_manager;
  struct wp_viewporter *viewporter;
  struct wp_viewport *viewport;
  struct wp_fractional_scale_manager_v1 *fractional_scale_manager;
  struct wp_fractional_scale_v1 *fractional_scale;

  double scale;
  bool scale_changeable;
  bool scale_changed;
  int32_t screen_w;
  int32_t screen_h;
  int32_t window_w_logical;
  int32_t window_h_logical;
  uint32_t mods_locked;
  uint32_t mouse_serial;
  uint32_t keyboard_serial;
  bool resizable;
  bool resized;
  bool borderless;
  bool decorated;

  uint32_t last_key_ts;
  uint32_t last_key;
};

struct ikafb_window
{
  struct ikafb_platform_data data;
  struct ikafb_callbacks callbacks;

  int32_t window_x;
  int32_t window_y;
  int32_t window_w;
  int32_t window_h;
  int32_t mouse_x;
  int32_t mouse_y;
  int32_t ime_x;
  int32_t ime_y;
  int16_t keycodes[256];
  uint32_t mods;

  bool close;
};

struct ikafb_keysym_map
{
  int16_t keysym;
  int16_t value;
  // ascii value for text input
  int16_t codepoint;
  // ascii value for shifted input, 0 means same as codepoint
  int16_t codepoint_shift;
};

static const struct ikafb_keysym_map s_ikafb_keysym_map[] = {
  {1, IKAFB_KEY_ESCAPE, 0, 0},
  {15, IKAFB_KEY_TAB, 0, 0},
  {42, IKAFB_KEY_LEFT_SHIFT, 0, 0},
  {54, IKAFB_KEY_RIGHT_SHIFT, 0, 0},
  {29, IKAFB_KEY_LEFT_CONTROL, 0, 0},
  {97, IKAFB_KEY_RIGHT_CONTROL, 0, 0},
  {56, IKAFB_KEY_LEFT_ALT, 0, 0},
  {100, IKAFB_KEY_RIGHT_ALT, 0, 0},
  {125, IKAFB_KEY_LEFT_SUPER, 0, 0},
  {126, IKAFB_KEY_RIGHT_SUPER, 0, 0},
  {139, IKAFB_KEY_MENU, 0, 0},
  {69, IKAFB_KEY_NUM_LOCK, 0, 0},
  {58, IKAFB_KEY_CAPS_LOCK, 0, 0},
  {210, IKAFB_KEY_PRINT_SCREEN, 0, 0},
  {70, IKAFB_KEY_SCROLL_LOCK, 0, 0},
  {119, IKAFB_KEY_PAUSE, 0, 0},
  {111, IKAFB_KEY_DELETE, 0, 0},
  {14, IKAFB_KEY_BACKSPACE, 0, 0},
  {28, IKAFB_KEY_ENTER, 0, 0},
  {102, IKAFB_KEY_HOME, 0, 0},
  {107, IKAFB_KEY_END, 0, 0},
  {104, IKAFB_KEY_PAGE_UP, 0, 0},
  {109, IKAFB_KEY_PAGE_DOWN, 0, 0},
  {110, IKAFB_KEY_INSERT, 0, 0},
  {105, IKAFB_KEY_LEFT, 0, 0},
  {106, IKAFB_KEY_RIGHT, 0, 0},
  {108, IKAFB_KEY_DOWN, 0, 0},
  {103, IKAFB_KEY_UP, 0, 0},
  {59, IKAFB_KEY_F1, 0, 0},
  {60, IKAFB_KEY_F2, 0, 0},
  {61, IKAFB_KEY_F3, 0, 0},
  {62, IKAFB_KEY_F4, 0, 0},
  {63, IKAFB_KEY_F5, 0, 0},
  {64, IKAFB_KEY_F6, 0, 0},
  {65, IKAFB_KEY_F7, 0, 0},
  {66, IKAFB_KEY_F8, 0, 0},
  {67, IKAFB_KEY_F9, 0, 0},
  {68, IKAFB_KEY_F10, 0, 0},
  {87, IKAFB_KEY_F11, 0, 0},
  {88, IKAFB_KEY_F12, 0, 0},
  {183, IKAFB_KEY_F13, 0, 0},
  {184, IKAFB_KEY_F14, 0, 0},
  {185, IKAFB_KEY_F15, 0, 0},
  {186, IKAFB_KEY_F16, 0, 0},
  {187, IKAFB_KEY_F17, 0, 0},
  {188, IKAFB_KEY_F18, 0, 0},
  {189, IKAFB_KEY_F19, 0, 0},
  {190, IKAFB_KEY_F20, 0, 0},
  {191, IKAFB_KEY_F21, 0, 0},
  {192, IKAFB_KEY_F22, 0, 0},
  {193, IKAFB_KEY_F23, 0, 0},
  {194, IKAFB_KEY_F24, 0, 0},
  {98, IKAFB_KEY_KP_DIVIDE, '/', 0},
  {55, IKAFB_KEY_KP_MULTIPLY, '*', 0},
  {74, IKAFB_KEY_KP_SUBTRACT, '-', 0},
  {78, IKAFB_KEY_KP_ADD, '+', 0},
  {83, IKAFB_KEY_KP_DECIMAL, '.', 0},
  {117, IKAFB_KEY_KP_EQUAL, '=', 0},
  {96, IKAFB_KEY_KP_ENTER, 0, 0},
  {82, IKAFB_KEY_KP_0, '0', 0},
  {79, IKAFB_KEY_KP_1, '1', 0},
  {80, IKAFB_KEY_KP_2, '2', 0},
  {81, IKAFB_KEY_KP_3, '3', 0},
  {75, IKAFB_KEY_KP_4, '4', 0},
  {76, IKAFB_KEY_KP_5, '5', 0},
  {77, IKAFB_KEY_KP_6, '6', 0},
  {71, IKAFB_KEY_KP_7, '7', 0},
  {72, IKAFB_KEY_KP_8, '8', 0},
  {73, IKAFB_KEY_KP_9, '9', 0},
  {16, IKAFB_KEY_Q, 'q', 'Q'},
  {17, IKAFB_KEY_W, 'w', 'W'},
  {18, IKAFB_KEY_E, 'e', 'E'},
  {19, IKAFB_KEY_R, 'r', 'R'},
  {20, IKAFB_KEY_T, 't', 'T'},
  {21, IKAFB_KEY_Y, 'y', 'Y'},
  {22, IKAFB_KEY_U, 'u', 'U'},
  {23, IKAFB_KEY_I, 'i', 'I'},
  {24, IKAFB_KEY_O, 'o', 'O'},
  {25, IKAFB_KEY_P, 'p', 'P'},
  {30, IKAFB_KEY_A, 'a', 'A'},
  {31, IKAFB_KEY_S, 's', 'S'},
  {32, IKAFB_KEY_D, 'd', 'D'},
  {33, IKAFB_KEY_F, 'f', 'F'},
  {34, IKAFB_KEY_G, 'g', 'G'},
  {35, IKAFB_KEY_H, 'h', 'H'},
  {36, IKAFB_KEY_J, 'j', 'J'},
  {37, IKAFB_KEY_K, 'k', 'K'},
  {38, IKAFB_KEY_L, 'l', 'L'},
  {44, IKAFB_KEY_Z, 'z', 'Z'},
  {45, IKAFB_KEY_X, 'x', 'X'},
  {46, IKAFB_KEY_C, 'c', 'C'},
  {47, IKAFB_KEY_V, 'v', 'V'},
  {48, IKAFB_KEY_B, 'b', 'B'},
  {49, IKAFB_KEY_N, 'n', 'N'},
  {50, IKAFB_KEY_M, 'm', 'M'},
  {2, IKAFB_KEY_1, '1', '!'},
  {3, IKAFB_KEY_2, '2', '@'},
  {4, IKAFB_KEY_3, '3', '#'},
  {5, IKAFB_KEY_4, '4', '$'},
  {6, IKAFB_KEY_5, '5', '%'},
  {7, IKAFB_KEY_6, '6', '^'},
  {8, IKAFB_KEY_7, '7', '&'},
  {9, IKAFB_KEY_8, '8', '*'},
  {10, IKAFB_KEY_9, '9', '('},
  {11, IKAFB_KEY_0, '0', ')'},
  {57, IKAFB_KEY_SPACE, ' ', 0},
  {12, IKAFB_KEY_MINUS, '-', '_'},
  {13, IKAFB_KEY_EQUAL, '=', '+'},
  {26, IKAFB_KEY_LEFT_BRACKET, '[', '{'},
  {27, IKAFB_KEY_RIGHT_BRACKET, ']', '}'},
  {43, IKAFB_KEY_BACKSLASH, '\\', '|'},
  {39, IKAFB_KEY_SEMICOLON, ';', ':'},
  {40, IKAFB_KEY_APOSTROPHE, '\'', '\"'},
  {41, IKAFB_KEY_GRAVE_ACCENT, '`', '~'},
  {51, IKAFB_KEY_COMMA, ',', '<'},
  {52, IKAFB_KEY_PERIOD, '.', '>'},
  {53, IKAFB_KEY_SLASH, '/', '?'},
};

static uint32_t
ikafb_get_timestamp(void)
{
#if defined(_POSIX_MONOTONIC_CLOCK)
  struct timespec time;
  clock_gettime(CLOCK_MONOTONIC, &time);
  uint32_t ts = (uint32_t)time.tv_sec * 1000 + time.tv_nsec / 1000000;
  return ts;
#else
  return 0;
#endif
}

// ids
static int32_t
ikafb_find_global_index_from_id(uint32_t ids[], uint32_t count, uint32_t id)
{
  for (uint32_t i = 0; i < count; ++i)
    if (ids[i] == id)
      return i;

  return -1;
}

static int32_t
ikafb_find_seat_index(struct ikafb_window *window, struct wl_seat *seat)
{
  for (uint32_t i = 0; i < window->data.seat_count; ++i)
    if (window->data.seat[i] == seat)
      return i;

  return -1;
}

static struct wl_seat *
ikafb_find_seat_for_pointer(struct ikafb_window *window,
                            struct wl_pointer *pointer)
{
  for (uint32_t i = 0; i < window->data.seat_count; ++i)
    if (window->data.pointer[i] == pointer)
      return window->data.seat[i];

  return NULL;
}

static bool
ikafb_add_output(struct ikafb_window *window, struct wl_output *output,
                 uint32_t id)
{
  if (window->data.output_count >= IKAFB_WL_MAX_OUTPUTS)
    return false;

  window->data.output[window->data.output_count] = output;
  window->data.output_id[window->data.output_count] = id;
  window->data.output_count++;
  return true;
}

static bool
ikafb_remove_output(struct ikafb_window *window, uint32_t id)
{
  int32_t idx = ikafb_find_global_index_from_id(window->data.output_id,
                                                window->data.output_count, id);
  if (idx < 0)
    return false;

  if (window->data.output[idx])
  {
    wl_output_destroy(window->data.output[idx]);
    window->data.output[idx] = NULL;
  }

  window->data.output[idx] = window->data.output[window->data.output_count];
  window->data.output_id[idx] =
    window->data.output_id[window->data.output_count];
  window->data.output_count--;
  return true;
}

static bool
ikafb_add_seat(struct ikafb_window *window, struct wl_seat *seat, uint32_t id)
{
  if (window->data.seat_count >= IKAFB_WL_MAX_SEATS)
    return false;

  window->data.seat[window->data.seat_count] = seat;
  window->data.seat_id[window->data.seat_count] = id;
  window->data.seat_count++;
  return true;
}

static bool
ikafb_remove_seat(struct ikafb_window *window, uint32_t id)
{
  int32_t idx = ikafb_find_global_index_from_id(window->data.seat_id,
                                                window->data.seat_count, id);
  if (idx < 0)
    return false;

  if (window->data.seat[idx])
  {
    wl_seat_destroy(window->data.seat[idx]);
    window->data.seat[idx] = NULL;
  }
  if (window->data.keyboard[idx])
  {
    wl_keyboard_destroy(window->data.keyboard[idx]);
    window->data.keyboard[idx] = NULL;
  }
  if (window->data.pointer[idx])
  {
    wl_pointer_destroy(window->data.pointer[idx]);
    window->data.pointer[idx] = NULL;
  }

  window->data.seat[idx] = window->data.seat[window->data.seat_count];
  window->data.seat_id[idx] = window->data.seat_id[window->data.seat_count];
  window->data.keyboard[idx] = window->data.keyboard[window->data.seat_count];
  window->data.pointer[idx] = window->data.pointer[window->data.seat_count];
  window->data.seat_count--;
  return true;
}

static int16_t
ikafb_translate_keysym(int keysym, const struct ikafb_keysym_map map[],
                       size_t size)
{
  for (size_t i = 0; i < size; ++i)
  {
    if (map[i].keysym == keysym)
      return map[i].value;
  }

  return IKAFB_KEY_UNKNOWN;
}

static int16_t
ikafb_translate_keysym_to_codepoint(int keysym, bool shift,
                                    const struct ikafb_keysym_map map[],
                                    size_t size)
{
  for (size_t i = 0; i < size; ++i)
  {
    if (map[i].keysym == keysym)
    {
      if (shift && map[i].codepoint_shift)
        return map[i].codepoint_shift;
      else
        return map[i].codepoint;
    }
  }

  return 0;
}

static void
ikafb_init_keycodes(struct ikafb_window *window)
{
  for (size_t i = 0; i < sizeof(window->keycodes) / sizeof(window->keycodes[0]);
       ++i)
    window->keycodes[i] = ikafb_translate_keysym(
      i, s_ikafb_keysym_map,
      sizeof(s_ikafb_keysym_map) / sizeof(s_ikafb_keysym_map[0]));
}

static void
buffer_release(void *data, struct wl_buffer *buffer)
{
  struct ikafb_window *window = data;
  window->data.shm_busy = false;
}

static const struct wl_buffer_listener buffer_listener = {
  buffer_release,
};

static bool
ikafb_create_shm_buffer(struct ikafb_window *window, int32_t w, int32_t h,
                        int32_t format)
{
  char *path = getenv("XDG_RUNTIME_DIR");
  char *template = "/ikafb-XXXXXX";
  if (!path)
    path = "/tmp";
  char *name = calloc(strlen(path) + strlen(template) + 1, 1);
  if (!name)
    goto fail;
  strcpy(name, path);
  strcat(name, template);

  window->data.shm_fd = mkstemp(name);
  unlink(name);
  free(name);
  if (window->data.shm_fd == -1)
    goto fail;
  long flags = fcntl(window->data.shm_fd, F_GETFD);
  if (flags == -1)
    goto cleanfd;
  fcntl(window->data.shm_fd, F_SETFD, flags | FD_CLOEXEC);

  window->data.shm_size = w * h * 4;
  if (ftruncate(window->data.shm_fd, window->data.shm_size) == -1)
    goto cleanfd;
  window->data.shm_data =
    mmap(NULL, window->data.shm_size, PROT_READ | PROT_WRITE, MAP_SHARED,
         window->data.shm_fd, 0);
  if (window->data.shm_data == MAP_FAILED)
    goto cleanfd;

  window->data.shm_pool = wl_shm_create_pool(
    window->data.shm, window->data.shm_fd, window->data.shm_size);
  window->data.shm_buffer =
    wl_shm_pool_create_buffer(window->data.shm_pool, 0, w, h, 4 * w, format);
  wl_buffer_add_listener(window->data.shm_buffer, &buffer_listener, window);
  return true;

cleanfd:
  close(window->data.shm_fd);
fail:
  return false;
}

static bool
ikafb_resize_shm_buffer(struct ikafb_window *window, int32_t w, int32_t h)
{
  size_t size = w * h * 4;
  ikafb_wait_for_buffer_release(window);
  wl_buffer_destroy(window->data.shm_buffer);

  if (size > window->data.shm_size)
  {
    // TODO: error handling
    wl_shm_pool_destroy(window->data.shm_pool);
    munmap(window->data.shm_data, window->data.shm_size);
    window->data.shm_size = size;
    ftruncate(window->data.shm_fd, window->data.shm_size);
    window->data.shm_data =
      mmap(NULL, window->data.shm_size, PROT_READ | PROT_WRITE, MAP_SHARED,
           window->data.shm_fd, 0);
    window->data.shm_pool = wl_shm_create_pool(
      window->data.shm, window->data.shm_fd, window->data.shm_size);
  }

  window->data.shm_buffer = wl_shm_pool_create_buffer(
    window->data.shm_pool, 0, w, h, 4 * w, window->data.shm_format);
  wl_buffer_add_listener(window->data.shm_buffer, &buffer_listener, window);
  wl_surface_attach(window->data.surface, window->data.shm_buffer, 0, 0);
  return true;
}

static int32_t
ikafb_convert_position(struct ikafb_window *window, double surface_pos)
{
  // hack around -ffast-math
  return round(surface_pos * window->data.scale + 0.00001);
}

// wayland callbacks
static void
xdg_surface_configure(void *data, struct xdg_surface *surface, uint32_t serial)
{
  struct ikafb_window *window = data;
  xdg_surface_ack_configure(surface, serial);

  if (!window->data.surface_configured)
  {
    window->data.surface_configured = true;
  }
}

static const struct xdg_surface_listener xdg_surface_listener = {
  .configure = xdg_surface_configure,
};

static void
xdg_toplevel_configure(void *data, struct xdg_toplevel *xdg_toplevel,
                       int32_t width, int32_t height, struct wl_array *state)
{
  struct ikafb_window *window = data;
  if (width == 0)
    width = window->data.window_w_logical;
  if (height == 0)
    height = window->data.window_h_logical;

  int32_t width_logical = width;
  int32_t height_logical = height;
  width = ikafb_convert_position(window, width);
  height = ikafb_convert_position(window, height);

  ikafb_wait_for_buffer_release(window);
  if (window->data.resizable)
  {
    window->data.window_w_logical = width_logical;
    window->data.window_h_logical = height_logical;
    window->window_w = width;
    window->window_h = height;
    window->data.resized = true;
  }
}

static void
xdg_toplevel_close(void *data, struct xdg_toplevel *xdg_toplevel)
{
  struct ikafb_window *window = data;
  window->close = true;
  callback(close_callback, 0);
}

static const struct xdg_toplevel_listener xdg_toplevel_listener = {
  xdg_toplevel_configure,
  xdg_toplevel_close,
  NULL,
  NULL,
};

static void
xdg_wm_base_ping(void *data, struct xdg_wm_base *shell, uint32_t serial)
{
  xdg_wm_base_pong(shell, serial);
}

static const struct xdg_wm_base_listener xdg_wm_base_listener = {
  xdg_wm_base_ping,
};

static void
keyboard_keymap(void *data, struct wl_keyboard *keyboard, uint32_t format,
                int fd, uint32_t size)
{
  close(fd);
}

static void
keyboard_enter(void *data, struct wl_keyboard *keyboard, uint32_t serial,
               struct wl_surface *surface, struct wl_array *keys)
{
  ;
}

static void
keyboard_leave(void *data, struct wl_keyboard *keyboard, uint32_t serial,
               struct wl_surface *surface)
{
  struct ikafb_window *window = data;
  window->data.last_key = 0;
  window->data.last_key_ts = 0;
}

static void
keyboard_modifiers(void *data, struct wl_keyboard *keyboard, uint32_t serial,
                   uint32_t mods_depressed, uint32_t mods_latched,
                   uint32_t mods_locked, uint32_t group)
{
  struct ikafb_window *window = data;
  window->data.mods_locked = mods_locked;
  window->mods = ((mods_depressed & 1) ? IKAFB_MOD_SHIFT : 0)
               | ((mods_depressed & 2) ? IKAFB_MOD_CAPS_LOCK : 0)
               | ((mods_depressed & 4) ? IKAFB_MOD_CONTROL : 0)
               | ((mods_depressed & 8) ? IKAFB_MOD_ALT : 0)
               | ((mods_depressed & 16) ? IKAFB_MOD_NUM_LOCK : 0)
               | ((mods_depressed & 64) ? IKAFB_MOD_SUPER : 0);
}

static void
keyboard_input_key(struct ikafb_window *window, uint32_t key, bool is_pressed)
{
  if (key < sizeof(window->keycodes) / sizeof(window->keycodes[0]))
  {
    int16_t keycode = window->keycodes[key];
    callback(keyboard_callback, keycode,
             window->mods | (is_pressed ? IKAFB_IS_PRESSED : 0));
    // FIXME: caps lock + nonletter keys, nums lock
    if (is_pressed
        && !(window->mods
             & (IKAFB_MOD_ALT | IKAFB_MOD_CONTROL | IKAFB_MOD_SUPER)))
    {
      bool caps_locked = window->data.mods_locked & 2;
      bool shift = window->mods & IKAFB_MOD_SHIFT;
      int16_t codepoint = ikafb_translate_keysym_to_codepoint(
        key, caps_locked ^ shift, s_ikafb_keysym_map,
        sizeof(s_ikafb_keysym_map) / sizeof(s_ikafb_keysym_map[0]));
      if (codepoint)
      {
        callback(textinput_callback, codepoint);
      }
    }
  }
}

static void
keyboard_key(void *data, struct wl_keyboard *keyboard, uint32_t serial,
             uint32_t time, uint32_t key, uint32_t state)
{
  struct ikafb_window *window = data;
  window->data.keyboard_serial = serial;
  bool is_pressed = state == WL_KEYBOARD_KEY_STATE_PRESSED;
  window->data.last_key = is_pressed ? key : 0;
  window->data.last_key_ts =
    is_pressed
      ? ikafb_get_timestamp() + IKAFB_AUTOREPEAT_DELAY - IKAFB_AUTOREPEAT_TIME
      : 0;
  keyboard_input_key(window, key, is_pressed);
}

static const struct wl_keyboard_listener keyboard_listener = {
  keyboard_keymap, keyboard_enter,     keyboard_leave,
  keyboard_key,    keyboard_modifiers, NULL,
};

static void
pointer_enter(void *data, struct wl_pointer *pointer, uint32_t serial,
              struct wl_surface *surface, wl_fixed_t sx, wl_fixed_t sy)
{
  struct ikafb_window *window = data;
  window->mouse_x = ikafb_convert_position(window, wl_fixed_to_double(sx));
  window->mouse_y = ikafb_convert_position(window, wl_fixed_to_double(sy));
  callback(mouse_move_callback, window->mouse_x, window->mouse_y);

  if (!window->data.cursor)
  {
    return;
  }
  struct wl_cursor_image *image = window->data.cursor->images[0];
  struct wl_buffer *buffer = wl_cursor_image_get_buffer(image);

  wl_pointer_set_cursor(pointer, serial, window->data.cursor_surface,
                        image->hotspot_x / window->data.cursor_scale,
                        image->hotspot_y / window->data.cursor_scale);
  wl_surface_attach(window->data.cursor_surface, buffer, 0, 0);
  wl_surface_damage(window->data.cursor_surface, 0, 0,
                    image->width / window->data.cursor_scale,
                    image->height / window->data.cursor_scale);
  wl_surface_commit(window->data.cursor_surface);
}

static void
pointer_leave(void *data, struct wl_pointer *pointer, uint32_t serial,
              struct wl_surface *surface)
{
  ;
}

static void
pointer_motion(void *data, struct wl_pointer *pointer, uint32_t time,
               wl_fixed_t sx, wl_fixed_t sy)
{
  struct ikafb_window *window = data;
  window->mouse_x = ikafb_convert_position(window, wl_fixed_to_double(sx));
  window->mouse_y = ikafb_convert_position(window, wl_fixed_to_double(sy));
  callback(mouse_move_callback, window->mouse_x, window->mouse_y);
}

static void
pointer_button(void *data, struct wl_pointer *pointer, uint32_t serial,
               uint32_t time, uint32_t button, uint32_t state)
{
  struct ikafb_window *window = data;
  window->data.mouse_serial = serial;
  window->data.seat_current = ikafb_find_seat_for_pointer(window, pointer);
  uint32_t btn = IKAFB_MOUSE_NONE;
  switch (button)
  {
  case 0x110:
    btn = IKAFB_MOUSE_LEFT;
    break;
  case 0x111:
    btn = IKAFB_MOUSE_RIGHT;
    break;
  case 0x112:
    btn = IKAFB_MOUSE_MIDDLE;
    break;
  }

  // TODO: proper begin resize api
  if (!window->data.decorated && btn == IKAFB_MOUSE_LEFT
      && state == WL_POINTER_BUTTON_STATE_PRESSED)
  {
    int32_t esize = 5 * window->data.scale;
    uint32_t edges = (window->mouse_x < esize ? 4 : 0)
                   | (window->mouse_x > window->window_w - esize ? 8 : 0)
                   | (window->mouse_y < esize ? 1 : 0)
                   | (window->mouse_y > window->window_h - esize ? 2 : 0);
    if (window->data.resizable && edges && window->data.seat_current)
    {
      xdg_toplevel_resize(window->data.xdg_toplevel, window->data.seat_current,
                          serial, edges);
      return;
    }
  }

  if (button != IKAFB_MOUSE_NONE)
  {
    callback(
      mouse_button_callback, btn,
      window->mods
        | (state == WL_POINTER_BUTTON_STATE_PRESSED ? IKAFB_IS_PRESSED : 0));
  }
}

static void
pointer_axis(void *data, struct wl_pointer *pointer, uint32_t time,
             uint32_t axis, wl_fixed_t value)
{
  struct ikafb_window *window = data;
  if (axis == 0)
  {
    double vf = wl_fixed_to_double(value) / 10.0;
    window->data.axis_value += vf;
    if (window->data.axis_value >= 1)
    {
      window->data.axis_value = 0;
      callback(mouse_button_callback, IKAFB_MOUSE_SCROLLDOWN,
               window->mods | IKAFB_IS_PRESSED);
    }
    else if (window->data.axis_value <= -1)
    {
      window->data.axis_value = 0;
      callback(mouse_button_callback, IKAFB_MOUSE_SCROLLUP,
               window->mods | IKAFB_IS_PRESSED);
    }
  }
}

static const struct wl_pointer_listener pointer_listener = {
  pointer_enter, pointer_leave, pointer_motion, pointer_button, pointer_axis,
  NULL,          NULL,          NULL,           NULL,
};

static void
seat_capabilities(void *data, struct wl_seat *seat,
                  enum wl_seat_capability caps)
{
  struct ikafb_window *window = data;
  int32_t idx = ikafb_find_seat_index(window, seat);
  if (idx < 0)
    return;

  if ((caps & WL_SEAT_CAPABILITY_KEYBOARD) && !window->data.keyboard[idx])
  {
    window->data.keyboard[idx] = wl_seat_get_keyboard(seat);
    wl_keyboard_add_listener(window->data.keyboard[idx], &keyboard_listener,
                             window);
  }
  else if (!(caps & WL_SEAT_CAPABILITY_KEYBOARD) && window->data.keyboard[idx])
  {
    wl_keyboard_destroy(window->data.keyboard[idx]);
    window->data.keyboard[idx] = NULL;
  }

  if ((caps & WL_SEAT_CAPABILITY_POINTER) && !window->data.pointer[idx])
  {
    window->data.pointer[idx] = wl_seat_get_pointer(seat);
    wl_pointer_add_listener(window->data.pointer[idx], &pointer_listener,
                            window);
  }
  else if (!(caps & WL_SEAT_CAPABILITY_POINTER) && window->data.pointer[idx])
  {
    wl_pointer_destroy(window->data.pointer[idx]);
    window->data.pointer[idx] = NULL;
  }
}

static const struct wl_seat_listener seat_listener = {
  seat_capabilities,
  NULL,
};

static void
output_geometry(void *data, struct wl_output *wl_output, int32_t x, int32_t y,
                int32_t phys_width, int32_t phys_height, int32_t subpixel,
                const char *make, const char *model, int32_t transform)
{
  ;
}

static void
output_mode(void *data, struct wl_output *wl_output, uint32_t flags,
            int32_t width, int32_t height, int32_t refresh)
{
  struct ikafb_window *window = data;
  if (!(flags & WL_OUTPUT_MODE_CURRENT))
    return;

  if (width * height > window->data.screen_w * window->data.screen_h)
  {
    window->data.screen_w = width;
    window->data.screen_h = height;
  }
}

static void
output_done(void *data, struct wl_output *wl_output)
{
  ;
}

static void
output_scale(void *data, struct wl_output *wl_output, int32_t factor)
{
  // TODO: make this unchangeable after init if not scale aware
  struct ikafb_window *window = data;
  if (factor > window->data.scale)
    window->data.scale = factor;
}

static const struct wl_output_listener output_listener = {
  output_geometry,
  output_mode,
  output_done,
  output_scale,
};

const char *mime_types[] = {
  "text/plain;charset=utf-8", "text/plain", "TEXT", "UTF8_STRING", "STRING",
};

#define IKAFB_FD_READABLE 1
#define IKAFB_FD_WRITEABLE 2
static int32_t
get_fd_status(int32_t fd, int32_t timeout_ms)
{
  int32_t ret = 0;
  struct timeval tv = {.tv_sec = 0, .tv_usec = timeout_ms * 1000};
  fd_set read_fds, write_fds;
  FD_ZERO(&read_fds);
  FD_ZERO(&write_fds);
  FD_SET(fd, &read_fds);
  FD_SET(fd, &write_fds);
  if (select(fd + 1, &read_fds, &write_fds, NULL, &tv) >= 0)
  {
    if (FD_ISSET(fd, &read_fds))
      ret |= IKAFB_FD_READABLE;
    if (FD_ISSET(fd, &write_fds))
      ret |= IKAFB_FD_WRITEABLE;
  }

  return ret;
}

static int32_t
get_data_offer_data(struct ikafb_window *window, struct wl_data_offer *id,
                    const char *mime_type, void *data, int32_t maxsize)
{
  int32_t fds[2] = {-1, -1};
  if (!id || pipe(fds) == -1)
  {
    return -1;
  }

  wl_data_offer_receive(id, mime_type, fds[1]);
  wl_display_flush(window->data.display);
  close(fds[1]);

  // HACK: if the fd isn't readable in 20 ms, give up
  int32_t ret = -1;
  if (get_fd_status(fds[0], 20) & IKAFB_FD_READABLE)
  {
    ret = read(fds[0], data, maxsize);
  }

  close(fds[0]);
  return ret;
}

static void
data_source_target(void *data, struct wl_data_source *wl_data_source,
                   const char *mime_type)
{
  ;
}

static void
data_source_send(void *data, struct wl_data_source *wl_data_source,
                 const char *mime_type, int32_t fd)
{
  struct ikafb_window *window = data;
  for (uint32_t i = 0; i < sizeof(mime_types) / sizeof(mime_types[0]); ++i)
  {
    if (strcmp(mime_types[i], mime_type) == 0 && window->data.clipboard)
    {
      if (get_fd_status(fd, 0) & IKAFB_FD_WRITEABLE)
      {
        write(fd, window->data.clipboard, strlen(window->data.clipboard));
      }
      break;
    }
  }

  close(fd);
}

static void
data_source_cancelled(void *data, struct wl_data_source *wl_data_source)
{
  // version 2 or older, only emitted if replaced by another data source
  struct ikafb_window *window = data;
  wl_data_source_destroy(wl_data_source);
  if (wl_data_source == window->data.data_source)
    window->data.data_source = NULL;
}

static const struct wl_data_source_listener data_source_listener = {
  data_source_target, data_source_send, data_source_cancelled, NULL, NULL, NULL,
};

static struct wl_data_source *
create_data_source(struct ikafb_window *window)
{
  if (!window->data.data_device_manager)
    return NULL;

  struct wl_data_source *data_source =
    wl_data_device_manager_create_data_source(window->data.data_device_manager);
  for (uint32_t i = 0; i < sizeof(mime_types) / sizeof(mime_types[0]); ++i)
  {
    wl_data_source_offer(data_source, mime_types[i]);
  }
  wl_data_source_add_listener(data_source, &data_source_listener, window);
  return data_source;
}

static void
data_device_data_offer(void *data, struct wl_data_device *wl_data_device,
                       struct wl_data_offer *id)
{
  ;
}

static void
data_device_enter(void *data, struct wl_data_device *wl_data_device,
                  uint32_t serial, struct wl_surface *surface, wl_fixed_t x,
                  wl_fixed_t y, struct wl_data_offer *id)
{
  // TODO: DND
  wl_data_offer_destroy(id);
}

static void
data_device_leave(void *data, struct wl_data_device *wl_data_device)
{
  ;
}

static void
data_device_motion(void *data, struct wl_data_device *wl_data_device,
                   uint32_t time, wl_fixed_t x, wl_fixed_t y)
{
  ;
}

static void
data_device_drop(void *data, struct wl_data_device *wl_data_device)
{
  ;
}

static void
data_device_selection(void *data, struct wl_data_device *wl_data_device,
                      struct wl_data_offer *id)
{
  struct ikafb_window *window = data;
  if (!id)
    return;

  void *text = calloc(4096, 1);
  if (text)
  {
    if (get_data_offer_data(window, id, "text/plain;charset=utf-8", text, 4090)
        >= 0)
    {
      free(window->data.clipboard);
      window->data.clipboard = text;
    }
    else
    {
      free(text);
    }
  }
  wl_data_offer_destroy(id);
}

static const struct wl_data_device_listener data_device_listener = {
  data_device_data_offer, data_device_enter, data_device_leave,
  data_device_motion,     data_device_drop,  data_device_selection,
};

static void
toplevel_decoration_configure(
  void *data, struct zxdg_toplevel_decoration_v1 *zxdg_toplevel_decoration_v1,
  uint32_t mode)
{
  struct ikafb_window *window = data;
  window->data.decorated =
    (mode == ZXDG_TOPLEVEL_DECORATION_V1_MODE_SERVER_SIDE);
  if (!window->data.borderless && !window->data.decorated)
  {
    fputs("WARNING: requested decoration but set as borderless", stderr);
  }
}

static const struct zxdg_toplevel_decoration_v1_listener
  toplevel_decoration_listener = {
    toplevel_decoration_configure,
};

static void
fractional_scale_preferred_scale(
  void *data, struct wp_fractional_scale_v1 *wp_fractional_scale_v1,
  uint32_t scale)
{
  struct ikafb_window *window = data;
  window->data.scale = scale / 120.0;
  window->data.scale_changed = true;
  xdg_toplevel_configure(window, NULL, window->data.window_w_logical,
                         window->data.window_h_logical, NULL);
}

static const struct wp_fractional_scale_v1_listener fractional_scale_listener =
  {
    fractional_scale_preferred_scale,
};

static void
registry_global(void *data, struct wl_registry *registry, uint32_t id,
                const char *interface, uint32_t version)
{
  struct ikafb_window *window = data;
  if (strcmp(interface, wl_compositor_interface.name) == 0)
  {
    uint32_t ver = 3;
    if (version < 3)
    {
      ver = 1;
    }
    window->data.compositor =
      wl_registry_bind(registry, id, &wl_compositor_interface, ver);
  }
  else if (strcmp(interface, xdg_wm_base_interface.name) == 0)
  {
    window->data.wm_base =
      wl_registry_bind(registry, id, &xdg_wm_base_interface, 1);
    xdg_wm_base_add_listener(window->data.wm_base, &xdg_wm_base_listener, data);
  }
  else if (strcmp(interface, wl_shm_interface.name) == 0)
  {
    window->data.shm = wl_registry_bind(registry, id, &wl_shm_interface, 1);
  }
  else if (strcmp(interface, wl_seat_interface.name) == 0)
  {
    struct wl_seat *seat =
      wl_registry_bind(registry, id, &wl_seat_interface, 1);
    if (seat && ikafb_add_seat(window, seat, id))
    {
      wl_seat_add_listener(seat, &seat_listener, data);
    }
    else if (seat)
    {
      fputs("WARNING: number of seats too large!", stderr);
      wl_seat_destroy(seat);
    }
  }
  else if (strcmp(interface, wl_output_interface.name) == 0)
  {
    uint32_t ver = 2;
    if (version < 2)
    {
      ver = 1;
    }
    struct wl_output *output =
      wl_registry_bind(registry, id, &wl_output_interface, ver);
    if (output && ikafb_add_output(window, output, id))
    {
      wl_output_add_listener(output, &output_listener, data);
    }
    else if (output)
    {
      fputs("WARNING: number of output too large!", stderr);
      wl_output_destroy(output);
    }
  }
  else if (strcmp(interface, wl_data_device_manager_interface.name) == 0)
  {
    window->data.data_device_manager =
      wl_registry_bind(registry, id, &wl_data_device_manager_interface, 1);
  }
  else if (strcmp(interface, zxdg_decoration_manager_v1_interface.name) == 0)
  {
    window->data.decoration_manager =
      wl_registry_bind(registry, id, &zxdg_decoration_manager_v1_interface, 1);
  }
  else if (strcmp(interface, wp_viewporter_interface.name) == 0)
  {
    window->data.viewporter =
      wl_registry_bind(registry, id, &wp_viewporter_interface, 1);
  }
  else if (window->data.scale_changeable
           && strcmp(interface, wp_fractional_scale_manager_v1_interface.name)
                == 0)
  {
    window->data.fractional_scale_manager = wl_registry_bind(
      registry, id, &wp_fractional_scale_manager_v1_interface, 1);
  }
}

static void
registry_global_remove(void *data, struct wl_registry *registry, uint32_t name)
{
  struct ikafb_window *window = data;
  ikafb_remove_output(window, name);
  ikafb_remove_seat(window, name);
}

static const struct wl_registry_listener registry_listener = {
  registry_global,
  registry_global_remove,
};

int32_t
ikafb_create_window(struct ikafb_window **_window, const char *title, int32_t x,
                    int32_t y, int32_t w, int32_t h, uint32_t flags)
{
  if (w <= 0 || h <= 0)
  {
    return IKAFB_RESULT_ERROR;
  }

  struct ikafb_window *window = calloc(1, sizeof(struct ikafb_window));
  if (!window)
  {
    return IKAFB_RESULT_ERROR;
  }

  window->data.scale = 1.0;
  window->data.display = wl_display_connect(NULL);
  if (!window->data.display)
  {
    free(window);
    return IKAFB_RESULT_ERROR;
  }

  window->data.registry = wl_display_get_registry(window->data.display);
  wl_registry_add_listener(window->data.registry, &registry_listener, window);
  wl_display_roundtrip(window->data.display);
  if (!window->data.shm || !window->data.compositor || !window->data.wm_base)
  {
    if (window->data.registry)
      wl_registry_destroy(window->data.registry);
    wl_display_disconnect(window->data.display);
    free(window);
    return IKAFB_RESULT_ERROR;
  }
  wl_display_roundtrip(window->data.display);

  int sw = w;
  int sh = h;

  if (flags & IKAFB_CW_FULLSCREEN)
  {
    x = 0;
    y = 0;
    w = sw;
    h = sh;
  }
  else if (flags & IKAFB_CW_CENTERED)
  {
    x = (sw - w) / 2;
    y = (sh - h) / 2;
  }

  if (flags & IKAFB_CW_RESIZABLE)
  {
    window->data.resizable = true;
  }

  if (flags & IKAFB_CW_BORDERLESS)
  {
    window->data.borderless = true;
  }

  if (flags & IKAFB_CW_SCALE_CHANGEABLE)
  {
    window->data.scale_changeable = true;
  }

  window->window_x = x;
  window->window_y = y;
  window->window_w = w;
  window->window_h = h;
  window->data.window_w_logical = round(w / window->data.scale);
  window->data.window_h_logical = round(h / window->data.scale);

  window->data.shm_format = (flags & IKAFB_CW_TRANSPARENT)
                            ? WL_SHM_FORMAT_ARGB8888
                            : WL_SHM_FORMAT_XRGB8888;
  if (!ikafb_create_shm_buffer(window, w, h, window->data.shm_format))
  {
    ikafb_destroy_window(window);
    return IKAFB_RESULT_ERROR;
  }

  window->data.cursor_scale = window->data.scale;
  window->data.cursor_theme = wl_cursor_theme_load(
    NULL, 24 * window->data.cursor_scale, window->data.shm);
  window->data.cursor =
    wl_cursor_theme_get_cursor(window->data.cursor_theme, "default");
  if (!window->data.cursor)
  {
    window->data.cursor =
      wl_cursor_theme_get_cursor(window->data.cursor_theme, "left_ptr");
  }
  if (!window->data.cursor)
  {
    fputs("WARNING: the cursor theme is broken!", stderr);
  }
  window->data.cursor_surface =
    wl_compositor_create_surface(window->data.compositor);
  wl_surface_set_buffer_scale(window->data.cursor_surface,
                              window->data.cursor_scale);

  window->data.surface = wl_compositor_create_surface(window->data.compositor);
  window->data.xdg_surface =
    xdg_wm_base_get_xdg_surface(window->data.wm_base, window->data.surface);
  xdg_surface_add_listener(window->data.xdg_surface, &xdg_surface_listener,
                           window);

  window->data.xdg_toplevel =
    xdg_surface_get_toplevel(window->data.xdg_surface);
  xdg_toplevel_add_listener(window->data.xdg_toplevel, &xdg_toplevel_listener,
                            window);
  xdg_toplevel_set_title(window->data.xdg_toplevel, title);
  xdg_toplevel_set_app_id(window->data.xdg_toplevel, title);
  wl_surface_commit(window->data.surface);

  if (flags & IKAFB_CW_FULLSCREEN)
  {
    xdg_toplevel_set_fullscreen(window->data.xdg_toplevel, NULL);
  }

  // TODO: DND
  // TODO: multi seat
  if (window->data.data_device_manager && window->data.seat[0])
  {
    signal(SIGPIPE, SIG_IGN);
    window->data.data_device = wl_data_device_manager_get_data_device(
      window->data.data_device_manager, window->data.seat[0]);
    wl_data_device_add_listener(window->data.data_device, &data_device_listener,
                                window);
  }

  if (window->data.viewporter)
  {
    window->data.viewport =
      wp_viewporter_get_viewport(window->data.viewporter, window->data.surface);
  }

  if (window->data.fractional_scale_manager)
  {
    window->data.fractional_scale =
      wp_fractional_scale_manager_v1_get_fractional_scale(
        window->data.fractional_scale_manager, window->data.surface);
    wp_fractional_scale_v1_add_listener(window->data.fractional_scale,
                                        &fractional_scale_listener, window);
  }

  if (!window->data.borderless && window->data.decoration_manager)
  {
    window->data.toplevel_decoration =
      zxdg_decoration_manager_v1_get_toplevel_decoration(
        window->data.decoration_manager, window->data.xdg_toplevel);
    zxdg_toplevel_decoration_v1_add_listener(
      window->data.toplevel_decoration, &toplevel_decoration_listener, window);
    zxdg_toplevel_decoration_v1_set_mode(
      window->data.toplevel_decoration,
      ZXDG_TOPLEVEL_DECORATION_V1_MODE_SERVER_SIDE);
  }
  else if (!window->data.borderless)
  {
    fputs("WARNING: window decoration requested, but "
          "zxdg_decoration_manager_v1 is unsupported!",
          stderr);
  }
  wl_display_roundtrip(window->data.display);

  ikafb_init_keycodes(window);
  *_window = window;
  return IKAFB_RESULT_SUCCESS;
}

#define CHECK_WINDOW_INITIALIZED() \
  if (!window)                     \
  return IKAFB_RESULT_UNINITIALIZED
#define CHECK_WINDOW_INITIALIZED_NORET() \
  if (!window)                           \
  return
#define DESTROY_LIST(type, count)                \
  for (uint32_t i = 0; i < count; ++i)           \
  {                                              \
    if (window->data.type[i])                    \
    {                                            \
      wl_##type##_destroy(window->data.type[i]); \
      window->data.type[i] = NULL;               \
    }                                            \
  }

void
ikafb_destroy_window(struct ikafb_window *window)
{
  CHECK_WINDOW_INITIALIZED_NORET();
  window->close = true;
  if (window->data.shm)
    wl_shm_destroy(window->data.shm);
  if (window->data.wm_base)
    xdg_wm_base_destroy(window->data.wm_base);
  if (window->data.compositor)
    wl_compositor_destroy(window->data.compositor);
  if (window->data.data_device_manager)
    wl_data_device_manager_destroy(window->data.data_device_manager);
  if (window->data.decoration_manager)
    zxdg_decoration_manager_v1_destroy(window->data.decoration_manager);
  if (window->data.viewporter)
    wp_viewporter_destroy(window->data.viewporter);
  if (window->data.fractional_scale_manager)
    wp_fractional_scale_manager_v1_destroy(
      window->data.fractional_scale_manager);
  if (window->data.registry)
    wl_registry_destroy(window->data.registry);

  DESTROY_LIST(output, window->data.output_count);
  DESTROY_LIST(seat, window->data.seat_count);
  DESTROY_LIST(keyboard, window->data.seat_count);
  DESTROY_LIST(pointer, window->data.seat_count);

  if (window->data.shm_pool)
    wl_shm_pool_destroy(window->data.shm_pool);
  if (window->data.cursor_theme)
    wl_cursor_theme_destroy(window->data.cursor_theme);
  if (window->data.surface)
    wl_surface_destroy(window->data.surface);
  if (window->data.cursor_surface)
    wl_surface_destroy(window->data.cursor_surface);
  if (window->data.shm_buffer)
    wl_buffer_destroy(window->data.shm_buffer);
  if (window->data.cursor_buffer)
    wl_buffer_destroy(window->data.cursor_buffer);
  if (window->data.xdg_surface)
    xdg_surface_destroy(window->data.xdg_surface);
  if (window->data.xdg_toplevel)
    xdg_toplevel_destroy(window->data.xdg_toplevel);
  if (window->data.data_device)
    wl_data_device_destroy(window->data.data_device);
  if (window->data.data_source)
    wl_data_source_destroy(window->data.data_source);
  if (window->data.viewport)
    wp_viewport_destroy(window->data.viewport);
  if (window->data.fractional_scale)
    wp_fractional_scale_v1_destroy(window->data.fractional_scale);
  if (window->data.toplevel_decoration)
    zxdg_toplevel_decoration_v1_destroy(window->data.toplevel_decoration);
  if (window->data.clipboard)
    free(window->data.clipboard);
  wl_display_flush(window->data.display);
  wl_display_disconnect(window->data.display);
  free(window);
}

static int32_t
ikafb_read_events(struct ikafb_window *window)
{
  CHECK_WINDOW_INITIALIZED();
  while (wl_display_prepare_read(window->data.display) != 0)
    wl_display_dispatch_pending(window->data.display);

  wl_display_flush(window->data.display);
  wl_display_read_events(window->data.display);
  return wl_display_dispatch_pending(window->data.display);
}

static int32_t
ikafb_wait_for_buffer_release(struct ikafb_window *window)
{
  CHECK_WINDOW_INITIALIZED();
  while (window->data.shm_busy)
  {
    if (ikafb_read_events(window) == -1)
      return IKAFB_RESULT_ERROR;
  }

  return IKAFB_RESULT_SUCCESS;
}

int32_t
ikafb_put_buffer_to_window(struct ikafb_window *window, int32_t dst_x,
                           int32_t dst_y, void *buffer, int32_t x, int32_t y,
                           int32_t w, int32_t h, uint32_t stride)
{
  CHECK_WINDOW_INITIALIZED();
  if (window->close)
  {
    return IKAFB_RESULT_WINDOW_CLOSED;
  }

  if (ikafb_wait_for_buffer_release(window) == IKAFB_RESULT_ERROR)
  {
    return IKAFB_RESULT_ERROR;
  }

  if (x < 0 || y < 0 || dst_x < 0 || dst_y < 0)
  {
    return IKAFB_RESULT_ERROR;
  }

  if (dst_x > window->window_w || dst_y > window->window_h)
  {
    return IKAFB_RESULT_SUCCESS;
  }

  if (!window->data.surface_configured)
  {
    return IKAFB_RESULT_SUCCESS;
  }

  if (dst_x + w > window->window_w)
    w = window->window_w - dst_x;
  if (dst_y + h > window->window_h)
    h = window->window_h - dst_y;

  for (int32_t j = 0; j < h; ++j)
  {
    uint32_t soffset = x * 4 + (j + y) * stride;
    uint32_t doffset = dst_x * 4 + (j + dst_y) * (window->window_w * 4);
    memcpy((char *)window->data.shm_data + doffset, (char *)buffer + soffset,
           w * 4);
  }

  int32_t x_logical = round(dst_x / window->data.scale);
  int32_t y_logical = round(dst_y / window->data.scale);
  int32_t w_logical = window->data.window_w_logical;
  int32_t h_logical = window->data.window_h_logical;
  if (window->data.viewport)
  {
    wp_viewport_set_destination(window->data.viewport, w_logical, h_logical);
  }
  else
  {
    wl_surface_set_buffer_scale(window->data.surface, window->data.scale);
  }

  window->data.shm_busy = true;
  wl_surface_attach(window->data.surface, window->data.shm_buffer, 0, 0);
  wl_surface_damage(window->data.surface, x_logical, y_logical, w_logical,
                    h_logical);
  wl_surface_commit(window->data.surface);

  return IKAFB_RESULT_SUCCESS;
}

int32_t
ikafb_process_events(struct ikafb_window *window)
{
  CHECK_WINDOW_INITIALIZED();
  if (window->close)
  {
    return IKAFB_RESULT_WINDOW_CLOSED;
  }

  bool has_event = false;
  int32_t events = ikafb_read_events(window);
  if (events == -1)
    return IKAFB_RESULT_ERROR;
  if (events > 0)
    has_event = true;

  if (window->data.last_key && window->data.last_key_ts)
  {
    uint32_t ts = ikafb_get_timestamp();
    if (ts > window->data.last_key_ts + IKAFB_AUTOREPEAT_TIME)
    {
      window->data.last_key_ts = ts;
      keyboard_input_key(window, window->data.last_key, true);
      has_event = true;
    }
  }

  if (window->data.resized)
  {
    if (ikafb_resize_shm_buffer(window, window->window_w, window->window_h))
    {
      callback(
        moveresize_callback, 0, 0, window->window_w, window->window_h,
        IKAFB_WINDOW_RESIZED
          | (window->data.scale_changed ? IKAFB_WINDOW_SCALE_CHANGED : 0));
      window->data.scale_changed = false;
    }
    window->data.resized = false;
  }

  return has_event ? IKAFB_RESULT_SUCCESS : IKAFB_RESULT_NO_EVENT;
}

void
ikafb_set_window_title(struct ikafb_window *window, const char *title)
{
  CHECK_WINDOW_INITIALIZED_NORET();
  xdg_toplevel_set_title(window->data.xdg_toplevel, title);
}

void
ikafb_accept_drag_and_drop(struct ikafb_window *window, bool accept)
{
  CHECK_WINDOW_INITIALIZED_NORET();
}

void
ikafb_get_window_position(struct ikafb_window *window, int32_t *x, int32_t *y,
                          int32_t *w, int32_t *h)
{
  *x = *y = *w = *h = 0;
  CHECK_WINDOW_INITIALIZED_NORET();
  *x = window->window_x;
  *y = window->window_y;
  *w = window->window_w;
  *h = window->window_h;
}

void
ikafb_get_mouse_position(struct ikafb_window *window, int32_t *x, int32_t *y)
{
  *x = *y = 0;
  CHECK_WINDOW_INITIALIZED_NORET();
  *x = window->mouse_x;
  *y = window->mouse_y;
}

// TODO: set window size
void
ikafb_set_window_position(struct ikafb_window *window, int32_t x, int32_t y,
                          int32_t w, int32_t h)
{
  CHECK_WINDOW_INITIALIZED_NORET();
}

void
ikafb_raise_window(struct ikafb_window *window)
{
  CHECK_WINDOW_INITIALIZED_NORET();
}

void
ikafb_begin_drag_window(struct ikafb_window *window)
{
  CHECK_WINDOW_INITIALIZED_NORET();
  // TODO: other mouse buttons
  callback(mouse_button_callback, IKAFB_MOUSE_LEFT, window->mods);
  xdg_toplevel_move(window->data.xdg_toplevel, window->data.seat_current,
                    window->data.mouse_serial);
}

void
ikafb_get_screen_size(struct ikafb_window *window, int32_t *w, int32_t *h)
{
  *w = *h = 0;
  CHECK_WINDOW_INITIALIZED_NORET();
  *w = window->data.screen_w;
  *h = window->data.screen_h;
}

void
ikafb_set_ime_position(struct ikafb_window *window, int32_t x, int32_t y)
{
  CHECK_WINDOW_INITIALIZED_NORET();
  window->ime_x = x;
  window->ime_y = y;
}

// scale only lite
struct scaleonly
{
  struct wl_output *output;
  int32_t factor;
};

static void
output_mode_scaleonly(void *data, struct wl_output *wl_output, uint32_t flags,
                      int32_t width, int32_t height, int32_t refresh)
{
  ;
}

static void
output_scaleonly(void *data, struct wl_output *wl_output, int32_t factor)
{
  struct scaleonly *scaleonly = data;
  if (factor > scaleonly->factor)
    scaleonly->factor = factor;
}

struct wl_output_listener output_listener_scaleonly = {
  output_geometry,
  output_mode_scaleonly,
  output_done,
  output_scaleonly,
};

static void
registry_global_scaleonly(void *data, struct wl_registry *registry, uint32_t id,
                          const char *interface, uint32_t version)
{
  struct scaleonly *scaleonly = data;
  if (strcmp(interface, wl_output_interface.name) == 0)
  {
    if (version < 2)
      return;
    scaleonly->output = wl_registry_bind(registry, id, &wl_output_interface, 2);
    wl_output_add_listener(scaleonly->output, &output_listener_scaleonly, data);
  }
}

static const struct wl_registry_listener registry_listener_scaleonly = {
  .global = registry_global_scaleonly,
  .global_remove = registry_global_remove,
};

void
ikafb_get_scale_factor(struct ikafb_window *window, float *scale)
{
  *scale = 1;
  if (!window)
  {
    struct scaleonly scaleonly = {NULL, 1};
    struct wl_display *display = wl_display_connect(NULL);
    if (!display)
      return;
    struct wl_registry *registry = wl_display_get_registry(display);
    wl_registry_add_listener(registry, &registry_listener_scaleonly,
                             &scaleonly);
    wl_display_roundtrip(display);
    wl_display_roundtrip(display);
    *scale = scaleonly.factor;
    wl_registry_destroy(registry);
    if (scaleonly.output)
      wl_output_destroy(scaleonly.output);
    wl_display_flush(display);
    wl_display_disconnect(display);
  }
  else
  {
    *scale = window->data.scale;
  }
}

void
ikafb_get_callbacks(struct ikafb_window *window,
                    struct ikafb_callbacks *callbacks)
{
  CHECK_WINDOW_INITIALIZED_NORET();
  *callbacks = window->callbacks;
}

void
ikafb_set_callbacks(struct ikafb_window *window,
                    struct ikafb_callbacks *callbacks)
{
  CHECK_WINDOW_INITIALIZED_NORET();
  window->callbacks = *callbacks;
}

static char *
ikafb_clipboard_strdup(const char *str)
{
  if (!str)
    return NULL;
  size_t l = strlen(str);
  char *d = calloc(l + 1, 1);
  if (!d)
    return NULL;
  return memcpy(d, str, l + 1);
}

int32_t
ikafb_set_clipboard_text(struct ikafb_window *window, const char *text)
{
  CHECK_WINDOW_INITIALIZED();

  if (window->data.data_device)
  {
    if (window->data.data_source)
    {
      wl_data_source_destroy(window->data.data_source);
    }
    window->data.data_source = create_data_source(window);
    if (window->data.data_source && window->data.keyboard_serial)
    {
      wl_data_device_set_selection(window->data.data_device,
                                   window->data.data_source,
                                   window->data.keyboard_serial);
      window->data.keyboard_serial = 0;
    }
    else if (window->data.data_source && window->data.mouse_serial)
    {
      wl_data_device_set_selection(window->data.data_device,
                                   window->data.data_source,
                                   window->data.mouse_serial);
      window->data.mouse_serial = 0;
    }
  }

  char *dup = ikafb_clipboard_strdup(text);
  if (!dup)
  {
    return IKAFB_RESULT_ERROR;
  }

  free(window->data.clipboard);
  window->data.clipboard = dup;
  return IKAFB_RESULT_SUCCESS;
}

char *
ikafb_get_clipboard_text(struct ikafb_window *window)
{
  if (!window)
  {
    return ikafb_clipboard_strdup("");
  }

  char *text = ikafb_clipboard_strdup(window->data.clipboard);
  if (!text)
  {
    text = ikafb_clipboard_strdup("");
  }

  return text;
}

bool
ikafb_has_clipboard_text(struct ikafb_window *window)
{
  bool result = false;
  char *text = ikafb_get_clipboard_text(window);
  if (text)
  {
    result = text[0] != '\0' ? true : false;
    free(text);
  }
  return result;
}
