# ikafb

Create window, read inputs, and draw pixels.

## Features
- Win32, X11, Wayland, Linux framebuffer (experimental), and dummy backends
- Easy to use API
- No global state
- HiDPI
- Clipboard and Drag-and-Drop
- Borderless and/or transparent window

## Usage
`#include "ikafb.h"`

`$CC program.c ikafb-win.c -lm -luser32 -lgdi32 -limm32 -o program`

`$CC program.c ikafb-x11.c -lm -lX11 -o program`

`$CC program.c ikafb-wl.c  -lm -lwayland-client -lwayland-cursor -o program`

`$CC program.c ikafb-dummy.c -o program`

