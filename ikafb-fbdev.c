#include "ikafb.h"
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <stdbool.h>
#include <unistd.h>
#include <limits.h>
#include <signal.h>

#include <linux/fb.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <sys/mman.h>
#include <sys/select.h>
#include <termios.h>

#define callback(type, ...)                                        \
  if (window->callbacks.type)                                      \
  {                                                                \
    window->callbacks.type(window, window->callbacks.type##_param, \
                           __VA_ARGS__);                           \
  }

struct ikafb_window
{
  struct ikafb_callbacks callbacks;

  int32_t window_x;
  int32_t window_y;
  int32_t window_w;
  int32_t window_h;
  int32_t mouse_x;
  int32_t mouse_y;
  int32_t ime_x;
  int32_t ime_y;
  int16_t keycodes[256];
  uint32_t mods;

  bool close;

  int32_t fb_fd;
  int32_t screen_w;
  int32_t screen_h;
  int32_t stride;
  uint8_t *pixels;
  int32_t term_fd;
};

struct ikafb_key_map
{
  char *sequence;
  int16_t value;
  // is text input
  int16_t printable;
  int16_t mods;
};

// HACK: swap alt and ctrl because more keys are available
#define CSI "\033["
static const struct ikafb_key_map s_ikafb_key_map[] = {
  {"\010", IKAFB_KEY_BACKSPACE, 0, 0},
  {"\011", IKAFB_KEY_TAB, 0, 0},
  {"\012", IKAFB_KEY_ENTER, 0, 0},
  {"\177", IKAFB_KEY_BACKSPACE, 0, 0},
  {CSI "A", IKAFB_KEY_UP, 0, 0},
  {CSI "B", IKAFB_KEY_DOWN, 0, 0},
  {CSI "C", IKAFB_KEY_RIGHT, 0, 0},
  {CSI "D", IKAFB_KEY_LEFT, 0, 0},
  {CSI "1~", IKAFB_KEY_HOME, 0, 0},
  {CSI "2~", IKAFB_KEY_INSERT, 0, 0},
  {CSI "3~", IKAFB_KEY_DELETE, 0, 0},
  {CSI "4~", IKAFB_KEY_END, 0, 0},
  {CSI "5~", IKAFB_KEY_PAGE_UP, 0, 0},
  {CSI "6~", IKAFB_KEY_PAGE_DOWN, 0, 0},
  {CSI "[A", IKAFB_KEY_F1, 0, 0},
  {CSI "[B", IKAFB_KEY_F2, 0, 0},
  {CSI "[C", IKAFB_KEY_F3, 0, 0},
  {CSI "[D", IKAFB_KEY_F4, 0, 0},
  {CSI "[E", IKAFB_KEY_F5, 0, 0},
  {CSI "17~", IKAFB_KEY_F6, 0, 0},
  {CSI "18~", IKAFB_KEY_F7, 0, 0},
  {CSI "19~", IKAFB_KEY_F8, 0, 0},
  {CSI "20~", IKAFB_KEY_F9, 0, 0},
  {CSI "21~", IKAFB_KEY_F10, 0, 0},
  {CSI "23~", IKAFB_KEY_F11, 0, 0},
  {CSI "24~", IKAFB_KEY_F12, 0, 0},
  {"\001", IKAFB_KEY_A, 0, IKAFB_MOD_ALT},
  {"\002", IKAFB_KEY_B, 0, IKAFB_MOD_ALT},
  {"\003", IKAFB_KEY_C, 0, IKAFB_MOD_ALT},
  {"\004", IKAFB_KEY_D, 0, IKAFB_MOD_ALT},
  {"\005", IKAFB_KEY_E, 0, IKAFB_MOD_ALT},
  {"\006", IKAFB_KEY_F, 0, IKAFB_MOD_ALT},
  {"\007", IKAFB_KEY_G, 0, IKAFB_MOD_ALT},
  {"\013", IKAFB_KEY_K, 0, IKAFB_MOD_ALT},
  {"\014", IKAFB_KEY_L, 0, IKAFB_MOD_ALT},
  {"\015", IKAFB_KEY_M, 0, IKAFB_MOD_ALT},
  {"\016", IKAFB_KEY_N, 0, IKAFB_MOD_ALT},
  {"\017", IKAFB_KEY_O, 0, IKAFB_MOD_ALT},
  {"\020", IKAFB_KEY_P, 0, IKAFB_MOD_ALT},
  {"\021", IKAFB_KEY_Q, 0, IKAFB_MOD_ALT},
  {"\022", IKAFB_KEY_R, 0, IKAFB_MOD_ALT},
  {"\023", IKAFB_KEY_S, 0, IKAFB_MOD_ALT},
  {"\024", IKAFB_KEY_T, 0, IKAFB_MOD_ALT},
  {"\025", IKAFB_KEY_U, 0, IKAFB_MOD_ALT},
  {"\026", IKAFB_KEY_V, 0, IKAFB_MOD_ALT},
  {"\027", IKAFB_KEY_W, 0, IKAFB_MOD_ALT},
  {"\030", IKAFB_KEY_X, 0, IKAFB_MOD_ALT},
  {"\031", IKAFB_KEY_Y, 0, IKAFB_MOD_ALT},
  {"\032", IKAFB_KEY_Z, 0, IKAFB_MOD_ALT},
  //{"\033",      IKAFB_KEY_3, 0, IKAFB_MOD_ALT},
  {"\034", IKAFB_KEY_4, 0, IKAFB_MOD_ALT},
  {"\035", IKAFB_KEY_5, 0, IKAFB_MOD_ALT},
  {"\036", IKAFB_KEY_6, 0, IKAFB_MOD_ALT},
  {"\037", IKAFB_KEY_7, 0, IKAFB_MOD_ALT},

  {" ", IKAFB_KEY_SPACE, 1, 0},
  {"'", IKAFB_KEY_APOSTROPHE, 1, 0},
  {",", IKAFB_KEY_COMMA, 1, 0},
  {"-", IKAFB_KEY_MINUS, 1, 0},
  {".", IKAFB_KEY_PERIOD, 1, 0},
  {"/", IKAFB_KEY_SLASH, 1, 0},
  {"0", IKAFB_KEY_0, 1, 0},
  {"1", IKAFB_KEY_1, 1, 0},
  {"2", IKAFB_KEY_2, 1, 0},
  {"3", IKAFB_KEY_3, 1, 0},
  {"4", IKAFB_KEY_4, 1, 0},
  {"5", IKAFB_KEY_5, 1, 0},
  {"6", IKAFB_KEY_6, 1, 0},
  {"7", IKAFB_KEY_7, 1, 0},
  {"8", IKAFB_KEY_8, 1, 0},
  {"9", IKAFB_KEY_9, 1, 0},
  {";", IKAFB_KEY_SEMICOLON, 1, 0},
  {"=", IKAFB_KEY_EQUAL, 1, 0},
  {"a", IKAFB_KEY_A, 1, 0},
  {"b", IKAFB_KEY_B, 1, 0},
  {"c", IKAFB_KEY_C, 1, 0},
  {"d", IKAFB_KEY_D, 1, 0},
  {"e", IKAFB_KEY_E, 1, 0},
  {"f", IKAFB_KEY_F, 1, 0},
  {"g", IKAFB_KEY_G, 1, 0},
  {"h", IKAFB_KEY_H, 1, 0},
  {"i", IKAFB_KEY_I, 1, 0},
  {"j", IKAFB_KEY_J, 1, 0},
  {"k", IKAFB_KEY_K, 1, 0},
  {"l", IKAFB_KEY_L, 1, 0},
  {"m", IKAFB_KEY_M, 1, 0},
  {"n", IKAFB_KEY_N, 1, 0},
  {"o", IKAFB_KEY_O, 1, 0},
  {"p", IKAFB_KEY_P, 1, 0},
  {"q", IKAFB_KEY_Q, 1, 0},
  {"r", IKAFB_KEY_R, 1, 0},
  {"s", IKAFB_KEY_S, 1, 0},
  {"t", IKAFB_KEY_T, 1, 0},
  {"u", IKAFB_KEY_U, 1, 0},
  {"v", IKAFB_KEY_V, 1, 0},
  {"w", IKAFB_KEY_W, 1, 0},
  {"x", IKAFB_KEY_X, 1, 0},
  {"y", IKAFB_KEY_Y, 1, 0},
  {"z", IKAFB_KEY_Z, 1, 0},
  {"[", IKAFB_KEY_LEFT_BRACKET, 1, 0},
  {"\\", IKAFB_KEY_BACKSLASH, 1, 0},
  {"]", IKAFB_KEY_RIGHT_BRACKET, 1, 0},
  {"`", IKAFB_KEY_GRAVE_ACCENT, 1, 0},

  {"\"", IKAFB_KEY_APOSTROPHE, 1, IKAFB_MOD_SHIFT},
  {"<", IKAFB_KEY_COMMA, 1, IKAFB_MOD_SHIFT},
  {"_", IKAFB_KEY_MINUS, 1, IKAFB_MOD_SHIFT},
  {">", IKAFB_KEY_PERIOD, 1, IKAFB_MOD_SHIFT},
  {"?", IKAFB_KEY_SLASH, 1, IKAFB_MOD_SHIFT},
  {")", IKAFB_KEY_0, 1, IKAFB_MOD_SHIFT},
  {"!", IKAFB_KEY_1, 1, IKAFB_MOD_SHIFT},
  {"@", IKAFB_KEY_2, 1, IKAFB_MOD_SHIFT},
  {"#", IKAFB_KEY_3, 1, IKAFB_MOD_SHIFT},
  {"$", IKAFB_KEY_4, 1, IKAFB_MOD_SHIFT},
  {"%", IKAFB_KEY_5, 1, IKAFB_MOD_SHIFT},
  {"^", IKAFB_KEY_6, 1, IKAFB_MOD_SHIFT},
  {"&", IKAFB_KEY_7, 1, IKAFB_MOD_SHIFT},
  {"*", IKAFB_KEY_8, 1, IKAFB_MOD_SHIFT},
  {"(", IKAFB_KEY_9, 1, IKAFB_MOD_SHIFT},
  {":", IKAFB_KEY_SEMICOLON, 1, IKAFB_MOD_SHIFT},
  {"+", IKAFB_KEY_EQUAL, 1, IKAFB_MOD_SHIFT},
  {"A", IKAFB_KEY_A, 1, IKAFB_MOD_SHIFT},
  {"B", IKAFB_KEY_B, 1, IKAFB_MOD_SHIFT},
  {"C", IKAFB_KEY_C, 1, IKAFB_MOD_SHIFT},
  {"D", IKAFB_KEY_D, 1, IKAFB_MOD_SHIFT},
  {"E", IKAFB_KEY_E, 1, IKAFB_MOD_SHIFT},
  {"F", IKAFB_KEY_F, 1, IKAFB_MOD_SHIFT},
  {"G", IKAFB_KEY_G, 1, IKAFB_MOD_SHIFT},
  {"H", IKAFB_KEY_H, 1, IKAFB_MOD_SHIFT},
  {"I", IKAFB_KEY_I, 1, IKAFB_MOD_SHIFT},
  {"J", IKAFB_KEY_J, 1, IKAFB_MOD_SHIFT},
  {"K", IKAFB_KEY_K, 1, IKAFB_MOD_SHIFT},
  {"L", IKAFB_KEY_L, 1, IKAFB_MOD_SHIFT},
  {"M", IKAFB_KEY_M, 1, IKAFB_MOD_SHIFT},
  {"N", IKAFB_KEY_N, 1, IKAFB_MOD_SHIFT},
  {"O", IKAFB_KEY_O, 1, IKAFB_MOD_SHIFT},
  {"P", IKAFB_KEY_P, 1, IKAFB_MOD_SHIFT},
  {"Q", IKAFB_KEY_Q, 1, IKAFB_MOD_SHIFT},
  {"R", IKAFB_KEY_R, 1, IKAFB_MOD_SHIFT},
  {"S", IKAFB_KEY_S, 1, IKAFB_MOD_SHIFT},
  {"T", IKAFB_KEY_T, 1, IKAFB_MOD_SHIFT},
  {"U", IKAFB_KEY_U, 1, IKAFB_MOD_SHIFT},
  {"V", IKAFB_KEY_V, 1, IKAFB_MOD_SHIFT},
  {"W", IKAFB_KEY_W, 1, IKAFB_MOD_SHIFT},
  {"X", IKAFB_KEY_X, 1, IKAFB_MOD_SHIFT},
  {"Y", IKAFB_KEY_Y, 1, IKAFB_MOD_SHIFT},
  {"Z", IKAFB_KEY_Z, 1, IKAFB_MOD_SHIFT},
  {"{", IKAFB_KEY_LEFT_BRACKET, 1, IKAFB_MOD_SHIFT},
  {"|", IKAFB_KEY_BACKSLASH, 1, IKAFB_MOD_SHIFT},
  {"}", IKAFB_KEY_RIGHT_BRACKET, 1, IKAFB_MOD_SHIFT},
  {"~", IKAFB_KEY_GRAVE_ACCENT, 1, IKAFB_MOD_SHIFT},
};

static bool
ikafb_init_fb_device(struct ikafb_window *window)
{
  window->fb_fd = open("/dev/fb0", O_RDWR | O_CLOEXEC);
  if (window->fb_fd == -1)
    return false;
  struct fb_var_screeninfo vinfo = {0};
  struct fb_fix_screeninfo finfo = {0};
  if (ioctl(window->fb_fd, FBIOGET_VSCREENINFO, &vinfo) == -1)
    goto cleanup;

  vinfo.bits_per_pixel = 32;
  vinfo.red.length = vinfo.green.length = vinfo.blue.length = 8;
  vinfo.transp.length = 0;
  vinfo.red.offset = 16;
  vinfo.green.offset = 8;
  vinfo.blue.offset = 0;
  if (ioctl(window->fb_fd, FBIOPUT_VSCREENINFO, &vinfo) == -1)
    goto cleanup;

  if (ioctl(window->fb_fd, FBIOGET_FSCREENINFO, &finfo) == -1)
    goto cleanup;

  fprintf(stderr,
          "line length: %u, res: %ux%u, bpp: %u, greyscale: %u, offset: %u %u "
          "%u, length: %u %u %u %u\n",
          finfo.line_length, vinfo.xres_virtual, vinfo.yres_virtual,
          vinfo.bits_per_pixel, vinfo.grayscale, vinfo.red.offset,
          vinfo.green.offset, vinfo.blue.offset, vinfo.red.length,
          vinfo.green.length, vinfo.blue.length, vinfo.transp.length);

  window->pixels = mmap(NULL, finfo.line_length * vinfo.yres_virtual,
                        PROT_READ | PROT_WRITE, MAP_SHARED, window->fb_fd, 0);
  if (window->pixels == MAP_FAILED)
    goto cleanup;

  window->screen_w = vinfo.xres_virtual;
  window->screen_h = vinfo.yres_virtual;
  window->stride = finfo.line_length;
  return true;
cleanup:
  close(window->fb_fd);
  return false;
}

static void
ikafb_close_fb_device(struct ikafb_window *window)
{
  munmap(window->pixels, window->stride * window->screen_h);
  close(window->fb_fd);
}

static sig_atomic_t reset_term_mode = 0;
// FIXME: this doesn't happen when fg a detached process
static void
sigcont_handler(int sig)
{
  reset_term_mode = 1;
}

static bool
ikafb_set_term_mode(struct ikafb_window *window, bool enable)
{
  struct termios ti = {0};
  if (tcgetattr(window->term_fd, &ti) == -1)
    return false;
  if (enable)
    ti.c_lflag &= ~(ECHO | ICANON);
  else
    ti.c_lflag |= (ECHO | ICANON);
  if (tcsetattr(window->term_fd, TCSANOW, &ti) == -1)
    return false;
  return true;
}

static bool
ikafb_init_term_device(struct ikafb_window *window)
{
  window->term_fd = open("/dev/tty", O_RDWR | O_CLOEXEC);
  if (window->term_fd == -1)
    return false;

  signal(SIGTTIN, SIG_IGN);
  signal(SIGTTOU, SIG_IGN);
  struct sigaction sa = {.sa_handler = sigcont_handler, .sa_flags = SA_RESTART};
  sigaction(SIGCONT, &sa, NULL);
  if (!ikafb_set_term_mode(window, true))
    goto cleanup;
  return true;
cleanup:
  close(window->term_fd);
  return false;
}

static void
ikafb_close_term_device(struct ikafb_window *window)
{
  if (window->term_fd == -1)
    return;

  signal(SIGTTIN, SIG_DFL);
  signal(SIGTTOU, SIG_DFL);
  signal(SIGCONT, SIG_DFL);
  if (!ikafb_set_term_mode(window, false))
    goto cleanup;
cleanup:
  close(window->term_fd);
}

#define IKAFB_FD_READABLE 1
#define IKAFB_FD_WRITEABLE 2
static int32_t
get_fd_status(int32_t fd, int32_t timeout_ms)
{
  int32_t ret = 0;
  struct timeval tv = {.tv_sec = 0, .tv_usec = timeout_ms * 1000};
  fd_set read_fds, write_fds;
  FD_ZERO(&read_fds);
  FD_ZERO(&write_fds);
  FD_SET(fd, &read_fds);
  FD_SET(fd, &write_fds);
  if (select(fd + 1, &read_fds, &write_fds, NULL, &tv) >= 0)
  {
    if (FD_ISSET(fd, &read_fds))
      ret |= IKAFB_FD_READABLE;
    if (FD_ISSET(fd, &write_fds))
      ret |= IKAFB_FD_WRITEABLE;
  }

  return ret;
}

static bool
ikafb_process_term_input(struct ikafb_window *window,
                         const struct ikafb_key_map map[], size_t size)
{
  if (reset_term_mode)
  {
    ikafb_set_term_mode(window, true);
    reset_term_mode = 0;
  }

  if ((get_fd_status(window->term_fd, 0) & IKAFB_FD_READABLE) == 0)
    return false;

  unsigned char buf[256] = {0};
  int32_t len = read(window->term_fd, buf, 255);
  if (len == -1)
    return false;

  int32_t cur = 0;
  while (cur < len)
  {
    int16_t mods = 0;
retry:;
    for (size_t i = 0; i < size; ++i)
    {
      if (!strncmp(map[i].sequence, (char *)buf + cur, strlen(map[i].sequence)))
      {
        callback(keyboard_callback, map[i].value,
                 mods | map[i].mods | IKAFB_IS_PRESSED);
        if (map[i].printable && !((mods | map[i].mods) & ~IKAFB_MOD_SHIFT))
          callback(textinput_callback, map[i].sequence[0]);
        callback(keyboard_callback, map[i].value, mods | map[i].mods);

        cur += strlen(map[i].sequence);
        goto match_done;
      }
    }

    if (buf[cur] == '\033')
    {
      cur += 1;
      mods = IKAFB_MOD_CONTROL;
      goto retry;
    }
match_done:;
  }

  return true;
}

int32_t
ikafb_create_window(struct ikafb_window **_window, const char *title, int32_t x,
                    int32_t y, int32_t w, int32_t h, uint32_t flags)
{
  if (w <= 0 || h <= 0)
  {
    return IKAFB_RESULT_ERROR;
  }

  struct ikafb_window *window = calloc(1, sizeof(struct ikafb_window));
  if (!window)
  {
    return IKAFB_RESULT_ERROR;
  }

  if (!ikafb_init_fb_device(window) || !ikafb_init_term_device(window))
  {
    return IKAFB_RESULT_ERROR;
  }

  int sw = window->screen_w;
  int sh = window->screen_h;

  if (flags & IKAFB_CW_FULLSCREEN)
  {
    x = 0;
    y = 0;
    w = sw;
    h = sh;
  }
  else if (flags & IKAFB_CW_CENTERED)
  {
    x = (sw - w) / 2;
    y = (sh - h) / 2;
  }

  ikafb_set_window_position(window, x, y, w, h);

  *_window = window;
  return IKAFB_RESULT_SUCCESS;
}

#define CHECK_WINDOW_INITIALIZED() \
  if (!window)                     \
  return IKAFB_RESULT_UNINITIALIZED
#define CHECK_WINDOW_INITIALIZED_NORET() \
  if (!window)                           \
  return
void
ikafb_destroy_window(struct ikafb_window *window)
{
  CHECK_WINDOW_INITIALIZED_NORET();
  window->close = true;
  ikafb_close_fb_device(window);
  ikafb_close_term_device(window);
  free(window);
}

int32_t
ikafb_put_buffer_to_window(struct ikafb_window *window, int32_t dst_x,
                           int32_t dst_y, void *buffer, int32_t x, int32_t y,
                           int32_t w, int32_t h, uint32_t stride)
{
  CHECK_WINDOW_INITIALIZED();
  if (window->close)
  {
    return IKAFB_RESULT_WINDOW_CLOSED;
  }

  if (x < 0 || y < 0 || dst_x < 0 || dst_y < 0)
  {
    return IKAFB_RESULT_ERROR;
  }

  dst_x += window->window_x;
  dst_y += window->window_y;

  if (dst_x > window->screen_w || dst_y > window->screen_h)
  {
    return IKAFB_RESULT_SUCCESS;
  }

  if (dst_x + w > window->screen_w)
    w = window->screen_w - dst_x;
  if (dst_y + h > window->screen_h)
    h = window->screen_h - dst_y;

  for (int32_t j = 0; j < h; ++j)
  {
    uint32_t soffset = x * 4 + (j + y) * stride;
    uint32_t doffset = dst_x * 4 + (j + dst_y) * window->stride;
    memcpy((char *)window->pixels + doffset, (char *)buffer + soffset, w * 4);
  }

  return IKAFB_RESULT_SUCCESS;
}

int32_t
ikafb_process_events(struct ikafb_window *window)
{
  CHECK_WINDOW_INITIALIZED();
  if (window->close)
  {
    return IKAFB_RESULT_WINDOW_CLOSED;
  }

  bool has_event = ikafb_process_term_input(window, s_ikafb_key_map,
                                            sizeof(s_ikafb_key_map)
                                              / sizeof(s_ikafb_key_map[0]));
  return has_event ? IKAFB_RESULT_SUCCESS : IKAFB_RESULT_NO_EVENT;
}

void
ikafb_set_window_title(struct ikafb_window *window, const char *title)
{
  CHECK_WINDOW_INITIALIZED_NORET();
}

void
ikafb_accept_drag_and_drop(struct ikafb_window *window, bool accept)
{
  CHECK_WINDOW_INITIALIZED_NORET();
}

void
ikafb_get_window_position(struct ikafb_window *window, int32_t *x, int32_t *y,
                          int32_t *w, int32_t *h)
{
  *x = *y = *w = *h = 0;
  CHECK_WINDOW_INITIALIZED_NORET();
  *x = window->window_x;
  *y = window->window_y;
  *w = window->window_w;
  *h = window->window_h;
}

void
ikafb_get_mouse_position(struct ikafb_window *window, int32_t *x, int32_t *y)
{
  *x = *y = 0;
  CHECK_WINDOW_INITIALIZED_NORET();
  *x = window->mouse_x;
  *y = window->mouse_y;
}

void
ikafb_set_window_position(struct ikafb_window *window, int32_t x, int32_t y,
                          int32_t w, int32_t h)
{
  CHECK_WINDOW_INITIALIZED_NORET();
  if (x >= 0 && y >= 0)
  {
    window->window_x = x;
    window->window_y = y;
  }
  window->window_w = w;
  window->window_h = h;
}

void
ikafb_raise_window(struct ikafb_window *window)
{
  CHECK_WINDOW_INITIALIZED_NORET();
}

void
ikafb_begin_drag_window(struct ikafb_window *window)
{
  CHECK_WINDOW_INITIALIZED_NORET();
}

void
ikafb_get_screen_size(struct ikafb_window *window, int32_t *w, int32_t *h)
{
  *w = *h = 0;
  CHECK_WINDOW_INITIALIZED_NORET();
  *w = window->screen_w;
  *h = window->screen_h;
}

void
ikafb_set_ime_position(struct ikafb_window *window, int32_t x, int32_t y)
{
  CHECK_WINDOW_INITIALIZED_NORET();
  window->ime_x = x;
  window->ime_y = y;
}

void
ikafb_get_scale_factor(struct ikafb_window *window, float *scale)
{
  *scale = 1;
}

void
ikafb_get_callbacks(struct ikafb_window *window,
                    struct ikafb_callbacks *callbacks)
{
  CHECK_WINDOW_INITIALIZED_NORET();
  *callbacks = window->callbacks;
}

void
ikafb_set_callbacks(struct ikafb_window *window,
                    struct ikafb_callbacks *callbacks)
{
  CHECK_WINDOW_INITIALIZED_NORET();
  window->callbacks = *callbacks;
}

int32_t
ikafb_set_clipboard_text(struct ikafb_window *window, const char *text)
{
  CHECK_WINDOW_INITIALIZED();
  return 0;
}

static char *
ikafb_clipboard_strdup(char *str)
{
  if (!str)
    return NULL;
  size_t l = strlen(str);
  char *d = calloc(l + 1, 1);
  if (!d)
    return NULL;
  return memcpy(d, str, l + 1);
}

char *
ikafb_get_clipboard_text(struct ikafb_window *window)
{
  if (!window)
  {
    return ikafb_clipboard_strdup("");
  }

  char *text = NULL;
  return text;
}

bool
ikafb_has_clipboard_text(struct ikafb_window *window)
{
  bool result = false;
  char *text = ikafb_get_clipboard_text(window);
  if (text)
  {
    result = text[0] != '\0' ? true : false;
    free(text);
  }
  return result;
}
