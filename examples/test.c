#define _POSIX_C_SOURCE 200809L
#include "ikafb.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sys/select.h>
#ifdef _WIN32
#define WIN32_LEAN_AND_MEAN
#include <windows.h>
#endif

struct app
{
  int w, h;
  int mouse_x, mouse_y;
  bool redraw;
  bool quit;
  char *pixels;
};

void
msleep(uint32_t ms)
{
#ifdef _WIN32
  Sleep(ms);
#else
  struct timeval tv;
  tv.tv_sec = (ms * 1000) / 1000000;
  tv.tv_usec = (ms * 1000) % 1000000;
  select(0, NULL, NULL, NULL, &tv);
#endif
}

void
redraw(struct ikafb_window *window, struct app *app)
{
  for (int y = 0; y < app->h; ++y)
    for (int x = 0; x < app->w; ++x)
    {
      size_t offset = app->w * 4 * y + 4 * x;
      app->pixels[offset + 0] = (app->mouse_x + x) & 255;
      app->pixels[offset + 1] = (app->mouse_y + y) & 255;
      app->pixels[offset + 2] = (app->mouse_x + app->mouse_y + x + y) & 255;
      app->pixels[offset + 3] = 255;
    }

  ikafb_put_buffer_to_window(window, 0, 0, app->pixels, 0, 0, app->w, app->h,
                             app->w * 4);
}

void
expose_callback(struct ikafb_window *window, void *param, int32_t reserved)
{
  struct app *app = param;
  app->redraw = true;
}

void
mouse_move_callback(struct ikafb_window *window, void *param, int32_t x,
                    int32_t y)
{
  struct app *app = param;
  app->mouse_x = x;
  app->mouse_y = y;
  app->redraw = true;
}

void
keyboard_callback(struct ikafb_window *window, void *param, uint32_t key,
                  uint32_t status)
{
  struct app *app = param;
  if (key == IKAFB_KEY_Q && IKAFB_PRESSED(status))
    app->quit = true;
}

int
main(void)
{
  int w = 300, h = 300;
  struct app app = {.w = 300, .h = 300, .pixels = calloc(300 * 300, 4)};
  struct ikafb_window *window = NULL;
  if (ikafb_create_window(&window, "test", 0, 0, w, h, IKAFB_CW_CENTERED)
      != IKAFB_RESULT_SUCCESS)
    return 1;

  struct ikafb_callbacks callbacks = {
    .expose_callback = expose_callback,
    .expose_callback_param = &app,
    .mouse_move_callback = mouse_move_callback,
    .mouse_move_callback_param = &app,
    .keyboard_callback = keyboard_callback,
    .keyboard_callback_param = &app,
  };
  ikafb_set_callbacks(window, &callbacks);
  redraw(window, &app);

  while (1)
  {
    int32_t res = ikafb_process_events(window);
    if (app.quit
        || (res != IKAFB_RESULT_SUCCESS && res != IKAFB_RESULT_NO_EVENT))
      break;

    if (app.redraw)
    {
      app.redraw = false;
      redraw(window, &app);
    }
    msleep(5);
  }

  ikafb_destroy_window(window);
  free(app.pixels);

  return 0;
}
