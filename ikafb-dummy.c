#include "ikafb.h"
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <stdbool.h>
#include <unistd.h>
#include <limits.h>

struct ikafb_window
{
  struct ikafb_callbacks callbacks;

  int32_t window_x;
  int32_t window_y;
  int32_t window_w;
  int32_t window_h;
  int32_t mouse_x;
  int32_t mouse_y;
  int32_t ime_x;
  int32_t ime_y;
  int16_t keycodes[256];
  uint32_t mods;

  bool close;
};

int32_t
ikafb_create_window(struct ikafb_window **_window, const char *title, int32_t x,
                    int32_t y, int32_t w, int32_t h, uint32_t flags)
{
  if (w <= 0 || h <= 0)
  {
    return IKAFB_RESULT_ERROR;
  }

  struct ikafb_window *window = calloc(1, sizeof(struct ikafb_window));
  if (!window)
  {
    return IKAFB_RESULT_ERROR;
  }

  int sw = w;
  int sh = h;

  if (flags & IKAFB_CW_FULLSCREEN)
  {
    x = 0;
    y = 0;
    w = sw;
    h = sh;
  }
  else if (flags & IKAFB_CW_CENTERED)
  {
    x = (sw - w) / 2;
    y = (sh - h) / 2;
  }

  window->window_x = x;
  window->window_y = y;
  window->window_w = w;
  window->window_h = h;

  *_window = window;
  return IKAFB_RESULT_SUCCESS;
}

#define CHECK_WINDOW_INITIALIZED() \
  if (!window)                     \
  return IKAFB_RESULT_UNINITIALIZED
#define CHECK_WINDOW_INITIALIZED_NORET() \
  if (!window)                           \
  return
void
ikafb_destroy_window(struct ikafb_window *window)
{
  CHECK_WINDOW_INITIALIZED_NORET();
  window->close = true;
  free(window);
}

int32_t
ikafb_put_buffer_to_window(struct ikafb_window *window, int32_t dst_x,
                           int32_t dst_y, void *buffer, int32_t x, int32_t y,
                           int32_t w, int32_t h, uint32_t stride)
{
  CHECK_WINDOW_INITIALIZED();
  if (window->close)
  {
    return IKAFB_RESULT_WINDOW_CLOSED;
  }

  return IKAFB_RESULT_SUCCESS;
}

int32_t
ikafb_process_events(struct ikafb_window *window)
{
  CHECK_WINDOW_INITIALIZED();
  if (window->close)
  {
    return IKAFB_RESULT_WINDOW_CLOSED;
  }

  bool has_event = false;
  return has_event ? IKAFB_RESULT_SUCCESS : IKAFB_RESULT_NO_EVENT;
}

void
ikafb_set_window_title(struct ikafb_window *window, const char *title)
{
  CHECK_WINDOW_INITIALIZED_NORET();
}

void
ikafb_accept_drag_and_drop(struct ikafb_window *window, bool accept)
{
  CHECK_WINDOW_INITIALIZED_NORET();
}

void
ikafb_get_window_position(struct ikafb_window *window, int32_t *x, int32_t *y,
                          int32_t *w, int32_t *h)
{
  *x = *y = *w = *h = 0;
  CHECK_WINDOW_INITIALIZED_NORET();
  *x = window->window_x;
  *y = window->window_y;
  *w = window->window_w;
  *h = window->window_h;
}

void
ikafb_get_mouse_position(struct ikafb_window *window, int32_t *x, int32_t *y)
{
  *x = *y = 0;
  CHECK_WINDOW_INITIALIZED_NORET();
  *x = window->mouse_x;
  *y = window->mouse_y;
}

void
ikafb_set_window_position(struct ikafb_window *window, int32_t x, int32_t y,
                          int32_t w, int32_t h)
{
  CHECK_WINDOW_INITIALIZED_NORET();
}

void
ikafb_raise_window(struct ikafb_window *window)
{
  CHECK_WINDOW_INITIALIZED_NORET();
}

void
ikafb_begin_drag_window(struct ikafb_window *window)
{
  CHECK_WINDOW_INITIALIZED_NORET();
}

void
ikafb_get_screen_size(struct ikafb_window *window, int32_t *w, int32_t *h)
{
  *w = *h = 0;
  CHECK_WINDOW_INITIALIZED_NORET();
}

void
ikafb_set_ime_position(struct ikafb_window *window, int32_t x, int32_t y)
{
  CHECK_WINDOW_INITIALIZED_NORET();
  window->ime_x = x;
  window->ime_y = y;
}

void
ikafb_get_scale_factor(struct ikafb_window *window, float *scale)
{
  *scale = 1;
}

void
ikafb_get_callbacks(struct ikafb_window *window,
                    struct ikafb_callbacks *callbacks)
{
  CHECK_WINDOW_INITIALIZED_NORET();
  *callbacks = window->callbacks;
}

void
ikafb_set_callbacks(struct ikafb_window *window,
                    struct ikafb_callbacks *callbacks)
{
  CHECK_WINDOW_INITIALIZED_NORET();
  window->callbacks = *callbacks;
}

int32_t
ikafb_set_clipboard_text(struct ikafb_window *window, const char *text)
{
  CHECK_WINDOW_INITIALIZED();
  return 0;
}

static char *
ikafb_clipboard_strdup(char *str)
{
  if (!str)
    return NULL;
  size_t l = strlen(str);
  char *d = calloc(l + 1, 1);
  if (!d)
    return NULL;
  return memcpy(d, str, l + 1);
}

char *
ikafb_get_clipboard_text(struct ikafb_window *window)
{
  if (!window)
  {
    return ikafb_clipboard_strdup("");
  }

  char *text = NULL;
  return text;
}

bool
ikafb_has_clipboard_text(struct ikafb_window *window)
{
  bool result = false;
  char *text = ikafb_get_clipboard_text(window);
  if (text)
  {
    result = text[0] != '\0' ? true : false;
    free(text);
  }
  return result;
}
