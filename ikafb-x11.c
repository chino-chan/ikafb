#include "ikafb.h"
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <stdbool.h>
#include <unistd.h>
#include <limits.h>
#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/keysym.h>
#include <X11/Xatom.h>

struct ikafb_platform_data
{
  Window window;
  Display *display;
  int screen;
  GC gc;
  Visual *visual;
  int depth;
  bool gc_allocated;

  int xdnd_version;
  Atom xdnd_req;
  Window xdnd_source;
};

struct ikafb_window
{
  struct ikafb_platform_data data;
  struct ikafb_callbacks callbacks;

  int32_t window_x;
  int32_t window_y;
  int32_t window_w;
  int32_t window_h;
  int32_t mouse_x;
  int32_t mouse_y;
  int32_t ime_x;
  int32_t ime_y;
  int16_t keycodes[256];
  uint32_t mods;

  bool close;
};

#define ATOM(s) ((Atom){XInternAtom(window->data.display, s, False)})

struct ikafb_keysym_map
{
  int keysym;
  int16_t value;
};

static const struct ikafb_keysym_map s_ikafb_numpad_keysym_map[] = {
  {XK_KP_0, IKAFB_KEY_0},
  {XK_KP_1, IKAFB_KEY_1},
  {XK_KP_2, IKAFB_KEY_2},
  {XK_KP_3, IKAFB_KEY_3},
  {XK_KP_4, IKAFB_KEY_4},
  {XK_KP_5, IKAFB_KEY_5},
  {XK_KP_6, IKAFB_KEY_6},
  {XK_KP_7, IKAFB_KEY_7},
  {XK_KP_8, IKAFB_KEY_8},
  {XK_KP_9, IKAFB_KEY_9},
  {XK_KP_Separator, IKAFB_KEY_PERIOD},
  {XK_KP_Decimal, IKAFB_KEY_PERIOD},
  {XK_KP_Equal, IKAFB_KEY_EQUAL},
  {XK_KP_Divide, IKAFB_KEY_SLASH},
  {XK_KP_Multiply, IKAFB_KEY_MULTIPLY},
  {XK_KP_Subtract, IKAFB_KEY_MINUS},
  {XK_KP_Add, IKAFB_KEY_ADD},
};

static const struct ikafb_keysym_map s_ikafb_level1_keysym_map[] = {
  {XK_KP_0, IKAFB_KEY_KP_0},
  {XK_KP_1, IKAFB_KEY_KP_1},
  {XK_KP_2, IKAFB_KEY_KP_2},
  {XK_KP_3, IKAFB_KEY_KP_3},
  {XK_KP_4, IKAFB_KEY_KP_4},
  {XK_KP_5, IKAFB_KEY_KP_5},
  {XK_KP_6, IKAFB_KEY_KP_6},
  {XK_KP_7, IKAFB_KEY_KP_7},
  {XK_KP_8, IKAFB_KEY_KP_8},
  {XK_KP_9, IKAFB_KEY_KP_9},
  {XK_KP_Separator, IKAFB_KEY_KP_DECIMAL},
  {XK_KP_Decimal, IKAFB_KEY_KP_DECIMAL},
  {XK_KP_Equal, IKAFB_KEY_KP_EQUAL},
  {XK_KP_Enter, IKAFB_KEY_KP_ENTER},
};

static const struct ikafb_keysym_map s_ikafb_level0_keysym_map[] = {
  {XK_Escape, IKAFB_KEY_ESCAPE},
  {XK_Tab, IKAFB_KEY_TAB},
  {XK_Shift_L, IKAFB_KEY_LEFT_SHIFT},
  {XK_Shift_R, IKAFB_KEY_RIGHT_SHIFT},
  {XK_Control_L, IKAFB_KEY_LEFT_CONTROL},
  {XK_Control_R, IKAFB_KEY_RIGHT_CONTROL},
  {XK_Meta_L, IKAFB_KEY_LEFT_ALT},
  {XK_Alt_L, IKAFB_KEY_LEFT_ALT},
  {XK_Mode_switch, IKAFB_KEY_RIGHT_ALT},
  {XK_ISO_Level3_Shift, IKAFB_KEY_RIGHT_ALT},
  {XK_Meta_R, IKAFB_KEY_RIGHT_ALT},
  {XK_Alt_R, IKAFB_KEY_RIGHT_ALT},
  {XK_Super_L, IKAFB_KEY_LEFT_SUPER},
  {XK_Super_R, IKAFB_KEY_RIGHT_SUPER},
  {XK_Menu, IKAFB_KEY_MENU},
  {XK_Num_Lock, IKAFB_KEY_NUM_LOCK},
  {XK_Caps_Lock, IKAFB_KEY_CAPS_LOCK},
  {XK_Print, IKAFB_KEY_PRINT_SCREEN},
  {XK_Scroll_Lock, IKAFB_KEY_SCROLL_LOCK},
  {XK_Pause, IKAFB_KEY_PAUSE},
  {XK_Delete, IKAFB_KEY_DELETE},
  {XK_BackSpace, IKAFB_KEY_BACKSPACE},
  {XK_Return, IKAFB_KEY_ENTER},
  {XK_Home, IKAFB_KEY_HOME},
  {XK_End, IKAFB_KEY_END},
  {XK_Page_Up, IKAFB_KEY_PAGE_UP},
  {XK_Page_Down, IKAFB_KEY_PAGE_DOWN},
  {XK_Insert, IKAFB_KEY_INSERT},
  {XK_Left, IKAFB_KEY_LEFT},
  {XK_Right, IKAFB_KEY_RIGHT},
  {XK_Down, IKAFB_KEY_DOWN},
  {XK_Up, IKAFB_KEY_UP},
  {XK_F1, IKAFB_KEY_F1},
  {XK_F2, IKAFB_KEY_F2},
  {XK_F3, IKAFB_KEY_F3},
  {XK_F4, IKAFB_KEY_F4},
  {XK_F5, IKAFB_KEY_F5},
  {XK_F6, IKAFB_KEY_F6},
  {XK_F7, IKAFB_KEY_F7},
  {XK_F8, IKAFB_KEY_F8},
  {XK_F9, IKAFB_KEY_F9},
  {XK_F10, IKAFB_KEY_F10},
  {XK_F11, IKAFB_KEY_F11},
  {XK_F12, IKAFB_KEY_F12},
  {XK_F13, IKAFB_KEY_F13},
  {XK_F14, IKAFB_KEY_F14},
  {XK_F15, IKAFB_KEY_F15},
  {XK_F16, IKAFB_KEY_F16},
  {XK_F17, IKAFB_KEY_F17},
  {XK_F18, IKAFB_KEY_F18},
  {XK_F19, IKAFB_KEY_F19},
  {XK_F20, IKAFB_KEY_F20},
  {XK_F21, IKAFB_KEY_F21},
  {XK_F22, IKAFB_KEY_F22},
  {XK_F23, IKAFB_KEY_F23},
  {XK_F24, IKAFB_KEY_F24},
  {XK_F25, IKAFB_KEY_F25},
  {XK_KP_Divide, IKAFB_KEY_KP_DIVIDE},
  {XK_KP_Multiply, IKAFB_KEY_KP_MULTIPLY},
  {XK_KP_Subtract, IKAFB_KEY_KP_SUBTRACT},
  {XK_KP_Add, IKAFB_KEY_KP_ADD},
  {XK_KP_Insert, IKAFB_KEY_KP_0},
  {XK_KP_End, IKAFB_KEY_KP_1},
  {XK_KP_Down, IKAFB_KEY_KP_2},
  {XK_KP_Page_Down, IKAFB_KEY_KP_3},
  {XK_KP_Left, IKAFB_KEY_KP_4},
  {XK_KP_Right, IKAFB_KEY_KP_6},
  {XK_KP_Home, IKAFB_KEY_KP_7},
  {XK_KP_Up, IKAFB_KEY_KP_8},
  {XK_KP_Page_Up, IKAFB_KEY_KP_9},
  {XK_KP_Delete, IKAFB_KEY_KP_DECIMAL},
  {XK_KP_Equal, IKAFB_KEY_KP_EQUAL},
  {XK_KP_Enter, IKAFB_KEY_KP_ENTER},
  {XK_a, IKAFB_KEY_A},
  {XK_b, IKAFB_KEY_B},
  {XK_c, IKAFB_KEY_C},
  {XK_d, IKAFB_KEY_D},
  {XK_e, IKAFB_KEY_E},
  {XK_f, IKAFB_KEY_F},
  {XK_g, IKAFB_KEY_G},
  {XK_h, IKAFB_KEY_H},
  {XK_i, IKAFB_KEY_I},
  {XK_j, IKAFB_KEY_J},
  {XK_k, IKAFB_KEY_K},
  {XK_l, IKAFB_KEY_L},
  {XK_m, IKAFB_KEY_M},
  {XK_n, IKAFB_KEY_N},
  {XK_o, IKAFB_KEY_O},
  {XK_p, IKAFB_KEY_P},
  {XK_q, IKAFB_KEY_Q},
  {XK_r, IKAFB_KEY_R},
  {XK_s, IKAFB_KEY_S},
  {XK_t, IKAFB_KEY_T},
  {XK_u, IKAFB_KEY_U},
  {XK_v, IKAFB_KEY_V},
  {XK_w, IKAFB_KEY_W},
  {XK_x, IKAFB_KEY_X},
  {XK_y, IKAFB_KEY_Y},
  {XK_z, IKAFB_KEY_Z},
  {XK_1, IKAFB_KEY_1},
  {XK_2, IKAFB_KEY_2},
  {XK_3, IKAFB_KEY_3},
  {XK_4, IKAFB_KEY_4},
  {XK_5, IKAFB_KEY_5},
  {XK_6, IKAFB_KEY_6},
  {XK_7, IKAFB_KEY_7},
  {XK_8, IKAFB_KEY_8},
  {XK_9, IKAFB_KEY_9},
  {XK_0, IKAFB_KEY_0},
  {XK_space, IKAFB_KEY_SPACE},
  {XK_minus, IKAFB_KEY_MINUS},
  {XK_equal, IKAFB_KEY_EQUAL},
  {XK_bracketleft, IKAFB_KEY_LEFT_BRACKET},
  {XK_bracketright, IKAFB_KEY_RIGHT_BRACKET},
  {XK_backslash, IKAFB_KEY_BACKSLASH},
  {XK_semicolon, IKAFB_KEY_SEMICOLON},
  {XK_apostrophe, IKAFB_KEY_APOSTROPHE},
  {XK_grave, IKAFB_KEY_GRAVE_ACCENT},
  {XK_comma, IKAFB_KEY_COMMA},
  {XK_period, IKAFB_KEY_PERIOD},
  {XK_slash, IKAFB_KEY_SLASH},
};

static int16_t
ikafb_translate_keysym(int keysym, const struct ikafb_keysym_map map[],
                       size_t size)
{
  for (size_t i = 0; i < size; ++i)
  {
    if (map[i].keysym == keysym)
      return map[i].value;
  }

  return IKAFB_KEY_UNKNOWN;
}

static void
ikafb_init_keycodes(struct ikafb_window *window)
{
  KeySym *keysyms;
  int min_keycode, max_keycode, keysyms_per_keycode;
  for (size_t i = 0; i < sizeof(window->keycodes) / sizeof(window->keycodes[0]);
       ++i)
    window->keycodes[i] = IKAFB_KEY_UNKNOWN;

  XDisplayKeycodes(window->data.display, &min_keycode, &max_keycode);
  keysyms =
    XGetKeyboardMapping(window->data.display, min_keycode,
                        max_keycode + 1 - min_keycode, &keysyms_per_keycode);

  for (int i = min_keycode; i <= max_keycode; ++i)
  {
    int keysym = keysyms[(i - min_keycode) * keysyms_per_keycode + 1];
    window->keycodes[i] = ikafb_translate_keysym(
      keysym, s_ikafb_level1_keysym_map,
      sizeof(s_ikafb_level1_keysym_map) / sizeof(s_ikafb_level1_keysym_map[0]));
    if (window->keycodes[i] == IKAFB_KEY_UNKNOWN)
    {
      keysym = keysyms[(i - min_keycode) * keysyms_per_keycode + 0];
      window->keycodes[i] =
        ikafb_translate_keysym(keysym, s_ikafb_level0_keysym_map,
                               sizeof(s_ikafb_level0_keysym_map)
                                 / sizeof(s_ikafb_level0_keysym_map[0]));
    }
  }

  XFree(keysyms);
}

int32_t
ikafb_create_window(struct ikafb_window **_window, const char *title, int32_t x,
                    int32_t y, int32_t w, int32_t h, uint32_t flags)
{
  if (w <= 0 || h <= 0)
  {
    return IKAFB_RESULT_ERROR;
  }

  int depth, nformats, bits_per_pixel = -1;
  XPixmapFormatValues *formats;
  XSetWindowAttributes swa;
  Visual *visual;
  Window root;

  struct ikafb_window *window = calloc(1, sizeof(struct ikafb_window));
  if (!window)
  {
    return IKAFB_RESULT_ERROR;
  }

  window->data.display = XOpenDisplay(NULL);
  if (!window->data.display)
  {
    free(window);
    return IKAFB_RESULT_ERROR;
  }

  window->data.screen = DefaultScreen(window->data.display);
  root = DefaultRootWindow(window->data.display);
  depth = DefaultDepth(window->data.display, window->data.screen);
  visual = DefaultVisual(window->data.display, window->data.screen);
  formats = XListPixmapFormats(window->data.display, &nformats);
  for (int i = 0; i < nformats; ++i)
  {
    if (depth == formats[i].depth)
    {
      bits_per_pixel = formats[i].bits_per_pixel;
      break;
    }
  }

  XFree(formats);
  if (bits_per_pixel != 32)
  {
    XCloseDisplay(window->data.display);
    free(window);
    return IKAFB_RESULT_ERROR;
  }

  swa.border_pixel = BlackPixel(window->data.display, window->data.screen);
  swa.backing_store = NotUseful;
  if (flags & IKAFB_CW_TRANSPARENT)
  {
    XVisualInfo vinfo;
    if (XMatchVisualInfo(window->data.display, window->data.screen, 32,
                         TrueColor, &vinfo))
    {
      visual = vinfo.visual;
      depth = vinfo.depth;
      swa.colormap =
        XCreateColormap(window->data.display, root, visual, AllocNone);
    }
  }

  int sw = DisplayWidth(window->data.display, window->data.screen);
  int sh = DisplayHeight(window->data.display, window->data.screen);

  if (flags & IKAFB_CW_FULLSCREEN)
  {
    x = 0;
    y = 0;
    w = sw;
    h = sh;
  }
  else if (flags & IKAFB_CW_CENTERED)
  {
    x = (sw - w) / 2;
    y = (sh - h) / 2;
  }

  window->window_x = x;
  window->window_y = y;
  window->window_w = w;
  window->window_h = h;
  window->data.visual = visual;
  window->data.depth = depth;
  window->data.window = XCreateWindow(
    window->data.display, root, x, y, w, h, 0, depth, InputOutput, visual,
    CWBorderPixel | CWBackingStore
      | ((flags & IKAFB_CW_TRANSPARENT) ? CWColormap : 0),
    &swa);

  if (!window->data.window)
  {
    XCloseDisplay(window->data.display);
    free(window);
    return IKAFB_RESULT_ERROR;
  }

  XSelectInput(window->data.display, window->data.window,
               KeyPressMask | KeyReleaseMask | ButtonPressMask
                 | ButtonReleaseMask | PointerMotionMask | StructureNotifyMask
                 | ExposureMask | FocusChangeMask);
  XStoreName(window->data.display, window->data.window, title);

  if (flags & IKAFB_CW_BORDERLESS)
  {
    long mwh[5] = {2, 0, 0, 0, 0};
    Atom mwh_p = ATOM("_MOTIF_WM_HINTS");
    XChangeProperty(window->data.display, window->data.window, mwh_p, mwh_p, 32,
                    PropModeReplace, (unsigned char *)&mwh, 5);
  }

  if (flags & IKAFB_CW_ALWAYS_ON_TOP)
  {
    Atom sa_atom = ATOM("_NET_WM_STATE_ABOVE");
    XChangeProperty(window->data.display, window->data.window,
                    ATOM("_NET_WM_STATE"), XA_ATOM, 32, PropModeReplace,
                    (unsigned char *)&sa_atom, 1);
  }

  if (flags & IKAFB_CW_FULLSCREEN)
  {
    Atom sf_atom = ATOM("_NET_WM_STATE_FULLSCREEN");
    XChangeProperty(window->data.display, window->data.window,
                    ATOM("_NET_WM_STATE"), XA_ATOM, 32, PropModeReplace,
                    (unsigned char *)&sf_atom, 1);
  }

  XSizeHints xsh;
  xsh.flags = PPosition | PMinSize | PMaxSize;
  xsh.x = 0;
  xsh.y = 0;
  xsh.min_width = w;
  xsh.min_height = h;
  if (flags & IKAFB_CW_RESIZABLE)
  {
    xsh.min_width = 1;
    xsh.min_height = 1;
    xsh.max_width = sw;
    xsh.max_height = sh;
  }
  else
  {
    xsh.max_width = w;
    xsh.max_height = h;
  }
  XSetWMNormalHints(window->data.display, window->data.window, &xsh);

  window->data.gc = DefaultGC(window->data.display, window->data.screen);
  if (flags & IKAFB_CW_TRANSPARENT)
  {
    XGCValues gcv;
    gcv.graphics_exposures = False;
    window->data.gc = XCreateGC(window->data.display, window->data.window,
                                GCGraphicsExposures, &gcv);
    window->data.gc_allocated = true;
  }

  XSetWMProtocols(window->data.display, window->data.window,
                  &ATOM("WM_DELETE_WINDOW"), 1);

  ikafb_init_keycodes(window);
  XClearWindow(window->data.display, window->data.window);
  XMapRaised(window->data.display, window->data.window);
  XFlush(window->data.display);

  *_window = window;
  return IKAFB_RESULT_SUCCESS;
}

#define CHECK_WINDOW_INITIALIZED() \
  if (!window)                     \
  return IKAFB_RESULT_UNINITIALIZED
#define CHECK_WINDOW_INITIALIZED_NORET() \
  if (!window)                           \
  return
void
ikafb_destroy_window(struct ikafb_window *window)
{
  CHECK_WINDOW_INITIALIZED_NORET();
  if (window->data.gc_allocated)
    XFreeGC(window->data.display, window->data.gc);

  if (window->data.window != None)
    XDestroyWindow(window->data.display, window->data.window);

  if (window->data.display)
    XCloseDisplay(window->data.display);

  window->close = true;
  free(window);
}

int32_t
ikafb_put_buffer_to_window(struct ikafb_window *window, int32_t dst_x,
                           int32_t dst_y, void *buffer, int32_t x, int32_t y,
                           int32_t w, int32_t h, uint32_t stride)
{
  CHECK_WINDOW_INITIALIZED();
  if (window->close)
  {
    return IKAFB_RESULT_WINDOW_CLOSED;
  }

  XImage *image =
    XCreateImage(window->data.display, window->data.visual, window->data.depth,
                 ZPixmap, 0, NULL, stride / 4, y + h, 32, stride);
  image->data = buffer;
  XPutImage(window->data.display, window->data.window, window->data.gc, image,
            x, y, dst_x, dst_y, w, h);
  XFlush(window->data.display);
  image->data = NULL;
  XDestroyImage(image);
  return IKAFB_RESULT_SUCCESS;
}

static int
ikafb_translate_key(struct ikafb_window *window, int keycode)
{
  if (keycode < 0 || keycode > 255)
    return IKAFB_KEY_UNKNOWN;

  return window->keycodes[keycode];
}

static int
ikafb_translate_mod(struct ikafb_window *window, int state)
{
  int mod_keys = 0;

  if (state & ShiftMask)
    mod_keys |= IKAFB_MOD_SHIFT;
  if (state & ControlMask)
    mod_keys |= IKAFB_MOD_CONTROL;
  if (state & Mod1Mask)
    mod_keys |= IKAFB_MOD_ALT;
  if (state & Mod4Mask)
    mod_keys |= IKAFB_MOD_SUPER;
  if (state & LockMask)
    mod_keys |= IKAFB_MOD_CAPS_LOCK;
  if (state & Mod2Mask)
    mod_keys |= IKAFB_MOD_NUM_LOCK;

  return mod_keys;
}

typedef struct
{
  unsigned char *data;
  int format, count;
  Atom type;
} ikafb_x11Prop;

static void
ikafb_read_window_property(ikafb_x11Prop *p, Display *disp, Window w, Atom prop)
{
  unsigned char *ret = NULL;
  Atom type;
  int fmt;
  unsigned long count, bytes_left;
  int bytes_fetch = 0;

  do
  {
    if (ret != 0)
      XFree(ret);
    XGetWindowProperty(disp, w, prop, 0, bytes_fetch, False, AnyPropertyType,
                       &type, &fmt, &count, &bytes_left, &ret);
    bytes_fetch += bytes_left;
  } while (bytes_left != 0);

  p->data = ret;
  p->format = fmt;
  p->count = count;
  p->type = type;
}

static Atom
ikafb_get_urilist_atom(Display *disp, Atom list[], int list_count)
{
  Atom request = None;
  char *name;
  for (int i = 0; i < list_count && request == None; i++)
  {
    name = XGetAtomName(disp, list[i]);
    if (strcmp("text/uri-list", name) == 0)
    {
      request = list[i];
    }
    XFree(name);
  }
  return request;
}

static int
ikafb_decode_uri(char *buf, int len)
{
  int ri, wi, di;
  char decode = '\0';
  if (buf == NULL || len < 0)
  {
    return -1;
  }
  if (len == 0)
  {
    len = strlen(buf);
  }
  for (ri = 0, wi = 0, di = 0; ri < len && wi < len; ri += 1)
  {
    if (di == 0)
    {
      /* start decoding */
      if (buf[ri] == '%')
      {
        decode = '\0';
        di += 1;
        continue;
      }
      /* normal write */
      buf[wi] = buf[ri];
      wi += 1;
      continue;
    }
    else if (di == 1 || di == 2)
    {
      char off = '\0';
      char isa = buf[ri] >= 'a' && buf[ri] <= 'f';
      char isA = buf[ri] >= 'A' && buf[ri] <= 'F';
      char isn = buf[ri] >= '0' && buf[ri] <= '9';
      if (!(isa || isA || isn))
      {
        /* not a hexadecimal */
        int sri;
        for (sri = ri - di; sri <= ri; sri += 1)
        {
          buf[wi] = buf[sri];
          wi += 1;
        }
        di = 0;
        continue;
      }
      /* itsy bitsy magicsy */
      if (isn)
      {
        off = 0 - '0';
      }
      else if (isa)
      {
        off = 10 - 'a';
      }
      else if (isA)
      {
        off = 10 - 'A';
      }
      decode |= (buf[ri] + off) << (2 - di) * 4;
      if (di == 2)
      {
        buf[wi] = decode;
        wi += 1;
        di = 0;
      }
      else
      {
        di += 1;
      }
      continue;
    }
  }
  buf[wi] = '\0';
  return wi;
}

static char *
ikafb_decode_uri_local(char *uri)
{
  char *file = NULL;
  bool local;

  if (memcmp(uri, "file:/", 6) == 0)
    uri += 6; /* local file? */
  else if (strstr(uri, ":/") != NULL)
    return file; /* wrong scheme */

  local = uri[0] != '/' || (uri[0] != '\0' && uri[1] == '/');

  /* got a hostname? */
  if (!local && uri[0] == '/' && uri[2] != '/')
  {
    char *hostname_end = strchr(uri + 1, '/');
    if (hostname_end != NULL)
    {
      char hostname[257];
      if (gethostname(hostname, 255) == 0)
      {
        hostname[256] = '\0';
        if (memcmp(uri + 1, hostname, hostname_end - (uri + 1)) == 0)
        {
          uri = hostname_end + 1;
          local = true;
        }
      }
    }
  }
  if (local)
  {
    file = uri;
    /* Convert URI escape sequences to real characters */
    ikafb_decode_uri(file, 0);
    if (uri[1] == '/')
    {
      file++;
    }
    else
    {
      file--;
    }
  }
  return file;
}

static void
ikafb_process_dnd_event(struct ikafb_window *window, XEvent *event)
{
  XClientMessageEvent m = {0};
  Display *display = window->data.display;

  if (event->xclient.message_type == ATOM("XdndEnter"))
  {
    bool use_list = event->xclient.data.l[1] & 1;
    window->data.xdnd_source = event->xclient.data.l[0];
    window->data.xdnd_version = (event->xclient.data.l[1] >> 24);

    if (use_list)
    {
      ikafb_x11Prop p;
      ikafb_read_window_property(&p, display, window->data.xdnd_source,
                                 ATOM("XdndTypeList"));
      window->data.xdnd_req =
        ikafb_get_urilist_atom(display, (Atom *)p.data, p.count);
      XFree(p.data);
    }
    else
    {
      Atom atoms[3] = {event->xclient.data.l[2], event->xclient.data.l[3],
                       event->xclient.data.l[4]};
      window->data.xdnd_req = ikafb_get_urilist_atom(display, atoms, 3);
    }
  }
  else if (event->xclient.message_type == ATOM("XdndPosition"))
  {
    m.type = ClientMessage;
    m.display = event->xclient.display;
    m.window = event->xclient.data.l[0];
    m.message_type = ATOM("XdndStatus");
    m.format = 32;
    m.data.l[0] = window->data.window;
    m.data.l[1] = (window->data.xdnd_req != None);
    m.data.l[2] = 0;
    m.data.l[3] = 0;
    m.data.l[4] = ATOM("XdndActionCopy");

    XSendEvent(display, event->xclient.data.l[0], False, NoEventMask,
               (XEvent *)&m);
    XFlush(display);
  }
  else if (event->xclient.message_type == ATOM("XdndDrop"))
  {
    if (window->data.xdnd_req == None)
    {
      m.type = ClientMessage;
      m.display = event->xclient.display;
      m.window = event->xclient.data.l[0];
      m.message_type = ATOM("XdndFinished");
      m.format = 32;
      m.data.l[0] = window->data.window;
      m.data.l[1] = 0;
      m.data.l[2] = None;
      XSendEvent(display, event->xclient.data.l[0], False, NoEventMask,
                 (XEvent *)&m);
    }
    else
    {
      XConvertSelection(display, ATOM("XdndSelection"), window->data.xdnd_req,
                        ATOM("PRIMARY"), window->data.window,
                        (window->data.xdnd_version >= 1)
                          ? event->xclient.data.l[2]
                          : CurrentTime);
    }
  }
}

#define callback(type, ...)                                        \
  if (window->callbacks.type)                                      \
  {                                                                \
    window->callbacks.type(window, window->callbacks.type##_param, \
                           __VA_ARGS__);                           \
  }
static void
ikafb_process_event(struct ikafb_window *window, XEvent *event)
{
  switch (event->type)
  {
  case KeyPress:
  case KeyRelease:
  {
    int keycode = ikafb_translate_key(window, event->xkey.keycode);
    window->mods = ikafb_translate_mod(window, event->xkey.state);
    callback(keyboard_callback, keycode,
             window->mods | ((event->type == KeyPress) ? IKAFB_IS_PRESSED : 0));

    if (event->type == KeyPress
        && !(window->mods
             & (IKAFB_MOD_ALT | IKAFB_MOD_CONTROL | IKAFB_MOD_SUPER)))
    {
      KeySym keysym;
      XLookupString(&event->xkey, NULL, 0, &keysym, NULL);
      int np_keysym =
        ikafb_translate_keysym(keysym, s_ikafb_numpad_keysym_map,
                               sizeof(s_ikafb_numpad_keysym_map)
                                 / sizeof(s_ikafb_numpad_keysym_map[0]));

      if ((keysym >= 0x0020 && keysym <= 0x007e)
          || (keysym >= 0x00a0 && keysym <= 0x00ff))
      {
        callback(textinput_callback, keysym);
      }
      else if ((keysym & 0xff000000) == 0x01000000)
      {
        keysym = keysym & 0x00ffffff;
        callback(textinput_callback, keysym);
      }
      else if (np_keysym != IKAFB_KEY_UNKNOWN)
      {
        keysym = np_keysym;
        callback(textinput_callback, keysym);
      }
    }

    break;
  }

  case ButtonPress:
  case ButtonRelease:
  {
    int button = IKAFB_MOUSE_NONE;
    switch (event->xbutton.button)
    {
    case Button1:
      button = IKAFB_MOUSE_LEFT;
      break;
    case Button2:
      button = IKAFB_MOUSE_MIDDLE;
      break;
    case Button3:
      button = IKAFB_MOUSE_RIGHT;
      break;
    case Button4:
      button = IKAFB_MOUSE_SCROLLUP;
      if (event->type == ButtonRelease)
        return;
      break;
    case Button5:
      button = IKAFB_MOUSE_SCROLLDOWN;
      if (event->type == ButtonRelease)
        return;
      break;
    default:
      break;
    }

    if (button != IKAFB_MOUSE_NONE)
    {
      callback(mouse_button_callback, button,
               window->mods
                 | ((event->type == ButtonPress) ? IKAFB_IS_PRESSED : 0));
    }

    break;
  }

  case MotionNotify:
  {
    window->mouse_x = event->xmotion.x;
    window->mouse_y = event->xmotion.y;
    callback(mouse_move_callback, event->xmotion.x, event->xmotion.y);
    break;
  }

  case ConfigureNotify:
  {
    uint32_t resized = (window->window_w != event->xconfigure.width
                        || window->window_h != event->xconfigure.height)
                       ? IKAFB_WINDOW_RESIZED
                       : 0;
    uint32_t moved = (window->window_x != event->xconfigure.x
                      || window->window_y != event->xconfigure.y)
                     ? IKAFB_WINDOW_MOVED
                     : 0;
    uint32_t status = resized | moved;
    window->window_x = event->xconfigure.x;
    window->window_y = event->xconfigure.y;
    window->window_w = event->xconfigure.width;
    window->window_h = event->xconfigure.height;
    callback(moveresize_callback, event->xconfigure.x, event->xconfigure.y,
             event->xconfigure.width, event->xconfigure.height, status);
    break;
  }

  case Expose:
  {
    callback(expose_callback, 0);
    break;
  }

  case DestroyNotify:
  {
    window->close = true;
    break;
  }

  case ClientMessage:
  {
    if ((Atom)event->xclient.data.l[0] == ATOM("WM_DELETE_WINDOW"))
    {
      window->close = true;
      callback(close_callback, 0);
      return;
    }

    ikafb_process_dnd_event(window, event);
    break;
  }

  case SelectionRequest:
  {
    const XSelectionRequestEvent *req = &event->xselectionrequest;
    XEvent sevent = {0};
    int seln_format;
    unsigned long nbytes;
    unsigned long overflow;
    unsigned char *seln_data;
    Display *display = window->data.display;

    sevent.xany.type = SelectionNotify;
    sevent.xselection.selection = req->selection;
    sevent.xselection.target = None;
    sevent.xselection.property = None;
    sevent.xselection.requestor = req->requestor;
    sevent.xselection.time = req->time;

    if (XGetWindowProperty(display, window->data.window,
                           ATOM("IKAFB_CUTBUFFER"), 0, INT_MAX / 4, False,
                           req->target, &sevent.xselection.target, &seln_format,
                           &nbytes, &overflow, &seln_data)
        == Success)
    {
      if (sevent.xselection.target == req->target)
      {
        XChangeProperty(display, req->requestor, req->property,
                        sevent.xselection.target, seln_format, PropModeReplace,
                        seln_data, nbytes);
        sevent.xselection.property = req->property;
      }
      else if (ATOM("TARGETS") == req->target)
      {
        Atom formats[] = {ATOM("TARGETS"), sevent.xselection.target};
        XChangeProperty(display, req->requestor, req->property, XA_ATOM, 32,
                        PropModeReplace, (unsigned char *)formats, 2);
        sevent.xselection.property = req->property;
        sevent.xselection.target = ATOM("TARGETS");
      }
      XFree(seln_data);
    }
    XSendEvent(display, req->requestor, False, 0, &sevent);
    break;
  }

  case SelectionNotify:
  {
    Display *display = window->data.display;
    Atom xdnd_req = window->data.xdnd_req;
    Window xdnd_source = window->data.xdnd_source;
    Atom target = event->xselection.target;
    if (target == xdnd_req)
    {
      ikafb_x11Prop p;
      ikafb_read_window_property(&p, display, window->data.window,
                                 ATOM("PRIMARY"));

      if (p.format == 8)
      {
        char *name = XGetAtomName(display, target);
        char *token = (char *)p.data;
        int count = 0;
        if (strcmp("text/uri-list", name) == 0)
        {
          while ((count = strcspn(token, "\r\n")))
          {
            token[count] = '\0';
            char *fn = ikafb_decode_uri_local(token);
            if (fn)
            {
              callback(drag_and_drop_callback, fn);
            }
            token += count + 1;
            token += strspn(token, "\r\n");
          }
        }
        XFree(name);
      }
      XFree(p.data);

      XClientMessageEvent m = {0};
      m.type = ClientMessage;
      m.display = display;
      m.window = xdnd_source;
      m.message_type = ATOM("XdndFinished");
      m.format = 32;
      m.data.l[0] = window->data.window;
      m.data.l[1] = 1;
      m.data.l[2] = ATOM("XdndActionCopy");
      XSendEvent(display, xdnd_source, False, NoEventMask, (XEvent *)&m);
      XSync(display, False);
    }
    break;
  }

  default:
    break;
  }
}

static bool
ikafb_process_once_typed_event(struct ikafb_window *window, int type)
{
  XEvent event;
  bool has_event = false;

  while ((window->close == false)
         && XCheckTypedEvent(window->data.display, type, &event))
  {
    has_event = true;
  }

  if (has_event)
    ikafb_process_event(window, &event);
  return has_event;
}

int32_t
ikafb_process_events(struct ikafb_window *window)
{
  CHECK_WINDOW_INITIALIZED();
  if (window->close)
  {
    return IKAFB_RESULT_WINDOW_CLOSED;
  }

  XEvent event;
  bool has_event = false;
  while ((window->close == false) && XPending(window->data.display))
  {
    bool has_once_typed_event =
      ikafb_process_once_typed_event(window, ConfigureNotify)
      || ikafb_process_once_typed_event(window, Expose);
    if (!has_once_typed_event)
    {
      XNextEvent(window->data.display, &event);
      ikafb_process_event(window, &event);
    }
    has_event = true;
  }

  return has_event ? IKAFB_RESULT_SUCCESS : IKAFB_RESULT_NO_EVENT;
}

void
ikafb_set_window_title(struct ikafb_window *window, const char *title)
{
  CHECK_WINDOW_INITIALIZED_NORET();
  XStoreName(window->data.display, window->data.window, title);
}

void
ikafb_accept_drag_and_drop(struct ikafb_window *window, bool accept)
{
  CHECK_WINDOW_INITIALIZED_NORET();
  Display *display = window->data.display;

  if (accept)
  {
    Atom xdnd_version = 5;
    XChangeProperty(display, window->data.window, ATOM("XdndAware"), XA_ATOM,
                    32, PropModeReplace, (unsigned char *)&xdnd_version, 1);
  }
  else
  {
    XDeleteProperty(display, window->data.window, ATOM("XdndAware"));
  }
}

void
ikafb_get_window_position(struct ikafb_window *window, int32_t *x, int32_t *y,
                          int32_t *w, int32_t *h)
{
  *x = *y = *w = *h = 0;
  CHECK_WINDOW_INITIALIZED_NORET();
  *x = window->window_x;
  *y = window->window_y;
  *w = window->window_w;
  *h = window->window_h;
}

void
ikafb_get_mouse_position(struct ikafb_window *window, int32_t *x, int32_t *y)
{
  *x = *y = 0;
  CHECK_WINDOW_INITIALIZED_NORET();
  *x = window->mouse_x;
  *y = window->mouse_y;
}

void
ikafb_set_window_position(struct ikafb_window *window, int32_t x, int32_t y,
                          int32_t w, int32_t h)
{
  CHECK_WINDOW_INITIALIZED_NORET();

  int xoff = 0, yoff = 0;
  Window root, parent, *children;
  unsigned int nchildren;
  XQueryTree(window->data.display, window->data.window, &root, &parent,
             &children, &nchildren);
  if (children)
    XFree(children);
  if (parent != root)
  {
    // assume the wm sets window position based on the top left corner of
    // decoration
    XWindowAttributes xwa;
    XGetWindowAttributes(window->data.display, window->data.window, &xwa);
    xoff = xwa.x;
    yoff = xwa.y;
  }
  XMoveResizeWindow(window->data.display, window->data.window, x - xoff,
                    y - yoff, w, h);
}

void
ikafb_raise_window(struct ikafb_window *window)
{
  CHECK_WINDOW_INITIALIZED_NORET();
  XRaiseWindow(window->data.display, window->data.window);
}

void
ikafb_begin_drag_window(struct ikafb_window *window)
{
  CHECK_WINDOW_INITIALIZED_NORET();
  // TODO: other mouse buttons
  callback(mouse_button_callback, IKAFB_MOUSE_LEFT, window->mods);

  Window root_return, child_return;
  int root_x_return = 0, root_y_return = 0;
  int win_x_return, win_y_return;
  unsigned int mask_return;
  XQueryPointer(window->data.display, window->data.window, &root_return,
                &child_return, &root_x_return, &root_y_return, &win_x_return,
                &win_y_return, &mask_return);
  XUngrabPointer(window->data.display, CurrentTime);
  XEvent xev = {
    .xclient = {.type = ClientMessage,
                .send_event = True,
                .message_type = ATOM("_NET_WM_MOVERESIZE"),
                .window = window->data.window,
                .format = 32,
                .data.l = {root_x_return, root_y_return, 8, 1, 1}},
  };
  XSendEvent(window->data.display, DefaultRootWindow(window->data.display),
             False, SubstructureRedirectMask | SubstructureNotifyMask, &xev);
}

void
ikafb_get_screen_size(struct ikafb_window *window, int32_t *w, int32_t *h)
{
  *w = *h = 0;
  CHECK_WINDOW_INITIALIZED_NORET();
  *w = DisplayWidth(window->data.display, window->data.screen);
  *h = DisplayHeight(window->data.display, window->data.screen);
}

void
ikafb_set_ime_position(struct ikafb_window *window, int32_t x, int32_t y)
{
  CHECK_WINDOW_INITIALIZED_NORET();
  window->ime_x = x;
  window->ime_y = y;
}

void
ikafb_get_scale_factor(struct ikafb_window *window, float *scale)
{
  *scale = 1;
}

void
ikafb_get_callbacks(struct ikafb_window *window,
                    struct ikafb_callbacks *callbacks)
{
  CHECK_WINDOW_INITIALIZED_NORET();
  *callbacks = window->callbacks;
}

void
ikafb_set_callbacks(struct ikafb_window *window,
                    struct ikafb_callbacks *callbacks)
{
  CHECK_WINDOW_INITIALIZED_NORET();
  window->callbacks = *callbacks;
}

int32_t
ikafb_set_clipboard_text(struct ikafb_window *window, const char *text)
{
  CHECK_WINDOW_INITIALIZED();
  Display *display = window->data.display;
  XChangeProperty(display, window->data.window, ATOM("IKAFB_CUTBUFFER"),
                  ATOM("UTF8_STRING"), 8, PropModeReplace,
                  (const unsigned char *)text, strlen(text));

  if (ATOM("CLIPBOARD") != None
      && XGetSelectionOwner(display, ATOM("CLIPBOARD")) != window->data.window)
  {
    XSetSelectionOwner(display, ATOM("CLIPBOARD"), window->data.window,
                       CurrentTime);
  }

  if (XGetSelectionOwner(display, ATOM("PRIMARY")) != window->data.window)
  {
    XSetSelectionOwner(display, ATOM("PRIMARY"), window->data.window,
                       CurrentTime);
  }
  return 0;
}

static char *
ikafb_clipboard_strdup(char *str)
{
  if (!str)
    return NULL;
  size_t l = strlen(str);
  char *d = calloc(l + 1, 1);
  if (!d)
    return NULL;
  return memcpy(d, str, l + 1);
}

char *
ikafb_get_clipboard_text(struct ikafb_window *window)
{
  if (!window)
  {
    return ikafb_clipboard_strdup("");
  }

  Display *display = window->data.display;
  Atom format;
  Window owner;
  Atom selection;
  Atom seln_type;
  XEvent notification;
  int seln_format;
  unsigned long nbytes;
  unsigned long overflow;
  unsigned char *src;
  char *text = NULL;
  if (ATOM("CLIPBOARD") == None)
  {
    return ikafb_clipboard_strdup("");
  }

  format = ATOM("UTF8_STRING");
  owner = XGetSelectionOwner(display, ATOM("CLIPBOARD"));
  if (owner == None)
  {
    owner = DefaultRootWindow(display);
    selection = XA_CUT_BUFFER0;
    format = XA_STRING;
  }
  else if (owner == window->data.window)
  {
    owner = window->data.window;
    selection = ATOM("IKAFB_CUTBUFFER");
  }
  else
  {
    owner = window->data.window;
    selection = ATOM("IKAFB_SELECTION");
    XConvertSelection(display, ATOM("CLIPBOARD"), format, selection, owner,
                      CurrentTime);

    while (!XCheckTypedWindowEvent(display, window->data.window,
                                   SelectionNotify, &notification))
    {
      while (!XPending(display))
        ;
    }
  }

  if (XGetWindowProperty(display, owner, selection, 0, INT_MAX / 4, False,
                         format, &seln_type, &seln_format, &nbytes, &overflow,
                         &src)
      == Success)
  {
    if (seln_type == format)
    {
      text = calloc(nbytes + 1, 1);
      if (text)
      {
        memcpy(text, src, nbytes);
        text[nbytes] = '\0';
      }
    }
    XFree(src);
  }

  if (!text)
  {
    text = ikafb_clipboard_strdup("");
  }

  return text;
}

bool
ikafb_has_clipboard_text(struct ikafb_window *window)
{
  bool result = false;
  char *text = ikafb_get_clipboard_text(window);
  if (text)
  {
    result = text[0] != '\0' ? true : false;
    free(text);
  }
  return result;
}
